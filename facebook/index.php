<?php
ini_set('display_errors',1);

date_default_timezone_set('UTC');
require_once './config.php';
$helper = $facebook->getRedirectLoginHelper();

$loginUrl = $helper->getLoginUrl(
	'http://' .
		$_SERVER['HTTP_HOST'] . '/facebook/callback.php',
	['user_photos', 'user_friends']
);

 ?>

<?php if(isset($_SESSION['facebook_access_token'])): ?>
<?php
	$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
	$profile_img_get = $facebook->get('me/?fields=picture.type(large){url}')->getGraphObject();
	$profile_name_get = $facebook->get('me/')->getGraphObject();
	$profile_name = $profile_name_get['name'];
	$profile_id = $profile_name_get['id'];
	$profile_img_url = $profile_img_get['picture']['url'];

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta charset="utf-8" />
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<title></title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<link rel="shortcut icon" href="favico.ico">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" type="text/css" media="screen, projection" />
		<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection" />
		<!--[if lte IE 6 ]><script type="text/javascript">window.location.href="ie6_close/index_ru.html";</script><![endif]-->
		<!--[if lt IE 9]> <link href= "css/ie8.css" rel= "stylesheet" media= "all" /> <![endif]-->

	<link rel="stylesheet" href="core-designer/nprogress.css">
	<link rel="stylesheet" href="core-designer/custom.css">

	<!-- Modals support -->
	<link rel="stylesheet" href="popups/css/bootstrap.css">
	<style>
		.func_button{
			cursor: default;
		}

		#func_plus{
			background-repeat:no-repeat;
			background-size:cover;
			background-image: url("core-designer/img/controls-plus-01.png");
		}
		:hover#func_plus{
			background-image: url("core-designer/img/controls-plus-01-hvr.png");
		}

		#func_minus{
			background-repeat:no-repeat;
			background-size:cover;
			background-image: url("core-designer/img/controls-minus-01.png");
		}
		:hover#func_minus{
			background-image: url("core-designer/img/controls-minus-01-hvr.png");
		}

		#func_del{
			background-repeat:no-repeat;
			background-size:cover;
			background-image: url("core-designer/img/controls-del-01.png");
		}
		:hover#func_del{
			background-image: url("core-designer/img/controls-del-01-hvr.png");
		}

		.next_load {
		    background-color: #5cb85c;
		    border: medium none;
		    border-radius: 0 !important;
		    color: white;
		    font-size: 12px;
		    font-weight: bold;
		    padding: 10px;
		    position: relative;
		    text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3);
		    width: 80%;
		    margin-left: 20px;
		}

		.backing{
			background: azure;
		    border-radius: 5px;
		    width: 118px;
		    height: 118px;
		}

		.drag{
    		background-repeat: no-repeat;
    		background-size: cover;
    		background-position: center center;
		}
	</style>
</head>

<body style="visibility:hidden;">
	<img style="display:none;"  src="core-designer/img/controls-plus-01.png">
	<img style="display:none;"  src="core-designer/img/controls-minus-01.png">
	<img style="display:none;"  src="core-designer/img/controls-del-01.png">
	<img style="display:none;"  src="core-designer/img/controls-plus-01-hvr.png">
	<img style="display:none;"  src="core-designer/img/controls-minus-01-hvr.png">
	<img style="display:none;"  src="core-designer/img/controls-del-01-hvr.png">
	<header id="header">
				<a href="/" class="logo"><img src="img/logo.png" alt=""></a>
				<div class="r-head">
						<div class="t1">Lieferung kostenlos</div>
						<div class="t2">Beratung<span>069-378-2928</span></div>
						<a href="/kontakt" class="bt1">Kontakt</a>
						<a href="/cart" class="bt2"></a>
				</div>
		</header>
		<section id="content">
				<div class="ll1">
						<div class="clmn1">
						    <div class="av1">
						        <div class="av-t1"><a href=""><div style="background-image:url('<?php print_r($profile_img_url); ?>');"></div><span><?=$profile_name?></span></a></div>
						        <!--<div class="av-t1"><a href=""><img src="<?=$profile_img_url?>" alt=""><span><?=$profile_name?></span></a></div>-->
						    </div>
						    <div class="content1">
						    </div>

							
						    <!-- .button-instagram -->
						    <a href="#" class="button-instagram">
						        Noch Bilder vom Instagram zeigen
						    </a>
						    <!-- /.button-instagram -->
						   
						</div>

						<div class="clmn2">
						    <div class="ov-clmn2">
							    <!-- .hint-photo -->
							    <div class="hint-photo">
							        Ziehe deine Fotos auf das Kissen
							    </div>
							    <!-- /.hint-photo -->

						        <div id="getphoto">
						            <div class="tit1"></div>
						            <div class="content2"></div>
						        </div>
						    </div>
						</div>
				</div>


				<div class="ll3">
				    <!-- .hint-grids -->
				    <div class="hint-grids">
				        wähle dein Kissen Motiv aus
				        <i class="hint-arrow"></i>
				    </div>
				    <!-- /.hint-grids -->
				    
				    <div class="clmn3">
				        <ul id="grid">
				            <li data-name="1 grosen Bild" data-grid="1" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/1.png"></li>
				            <li data-name="5 х 5 Bildern" data-grid="5" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/25.png"></li>
				            <li data-name="4 х 4 Bildren" data-grid="4" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/16.png"></li>
				            <li data-name="2 Bildern" data-grid="22" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/2.png"></li>
				            <li data-name="2 х 2 Bildern" data-grid="2" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/4.png"></li>
				            <li data-name="3 х 3 Bildern" data-grid="3" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/9.png"></li>
				            <li data-name="12 Kleine Bildern und 1 grossen Bild" data-grid="150" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/13.png"></li>
				            <li data-name="12 Kleine Bildern und 3 grossen Bildern" data-grid="120" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/15.png"></li>
				        </ul>
				    </div>
				</div>



		<div class="rr1">
		    <div class="dc1">
		        <div class="tit1" id="tit1" style="display:block;">
		            <?php // <span id="quant_bild">2 х 2 Bildern</span> ?>
		        </div>
		        <div class="wrapper">
		            <div id="designer">
		                <div id="main_object"></div>
		                <div id="drag_photo_image"></div>
		                <div id="main_object_load"></div>
		            </div>
		            <img id="background-image" src="img/podushka.png" />
		        </div>
		        <div class="tit2" id="tit2" style="display:block;"><span>39,00€</span> 40 cm x 40 cm bedrucktes Kissen</div>
		   	</div>
		</div>

		
	</section>


	<!-- <form id='form' method='POST' action='../product/add_handler.php'>
		<input type='hidden' name='thumb'>
		<input type='hidden' name='source'> -->
		<div class="ft-bot1">
			<a href="#" onclick="save()" id="save_design">weiter</a>
		</div>
	<!-- </form> -->

	<!-- Modal -->
	<div class="modal fade" id="modal1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
					<div class="loading-icon"></div>
				</div>
				<div class="modal-body">
					<h4 class="modal-title">Deine kissen wird erstellt...</h4>
					<p>Dauert ca. 1 Minute. Bitte das Fenster nicht schließen!</p>
				</div>
				<div class="modal-footer">
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- Modal end -->

	<!-- Modal -->
    <div class="modal fade" id="modal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-image: url(facebook/popups/img/modal2-header.jpg);">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title">Loge Dich bei Facebook um die Bilder hochzuladen!</h4>
                </div>
                <div class="modal-footer">
                    <div class="buttons-wrapper">
                        <a href="/facebook" class="modal-btn facebook"></a>
                        <?php // <a href="#" class="modal-btn instagram"></a> ?>
                        <?php // <a href="#" class="modal-btn drive">einfach Bilder von Festplatte hinzuf端gen</a> ?>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal end -->


	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
	<script src="core-designer/jqueryui.min.js"></script>
	<script src="core-designer/js/jquery.ui.touch-punch.js"></script>
	<script src="core-designer/upload/js/plupload.full.min.js"></script>
	<script src="core-designer/boot3.js"></script>
	<script src="core-designer/bootstrap.min.js"></script>
	<script src="core-designer/nprogress.js"></script>


	<!--<script src="js/jquery.js"></script>-->
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/draggable_background.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript">		
		NProgress.configure({
		    parent: '#modal1 .modal-footer',
		    spinnerSelector: '',
		});
		var href=document.location.href;
		var ind=href.indexOf("#");
		var st=(ind==-1?3:href.substr(ind+1));
		st=parseInt(st);
		selectedGridElementLi(null,st?st:3);	
		function save(){
			var frm=document.getElementById("form");
			$("#modal1").modal("show");
			NProgress.done();
			NProgress.start();
			var photos_arr=[];
			$(".obj").each(function(elem, index){
			    var albums_index=$(this).attr("albums_index"), photos_index=$(this).attr("photos_index");
			    var curr_width =parseInt($(this).css("width"))+1, curr_height=parseInt($(this).css("height"))+1;
			    var item = ALBUMS_DATA['albums'][albums_index]['images'][photos_index];
			    var x=parseInt($(this).css("left")), y=parseInt($(this).css("top"));
			    x==1?0:x;
			    y==1?0:y;
			    console.log(x,y);
			    photos_arr.push({"x":0,"y":0,"size_w": curr_width*8, "size_h": curr_height*8, "width":curr_height*(item.width/item.height)*8, "offset_x": x*8, "offset_y":y*8, "url":item.url});
			});
			console.log(photos_arr);
			save_designer(photos_arr);
		}	

		function save_designer(photos){
			$.ajax({
			    type:'POST',
			    url: 'save-designer.php',
			    cache: false,
			    data: {
			    	data:photos,
			        id: "<?=$profile_id?>"
			    },
			    success: function(response){
			      	response = eval("(" + response + ")");
			        console.log(response);
			        getId(response);
			    },
			    error: function() {
			        console.log('Ajax error:');
			        console.dir(arguments);
			    }
			}); 
		}

		function postId(id){
			$.ajax({
			    type: 'POST',
			    url: '../cart/', // ссылка на товар
			    data: 'add-to-cart='+id+'&quantity=1', // 10891 - ID товара
			    success: function(data){
			    	NProgress.done();
			      	document.location="http://instakissen.de/checkout/";
			    }
			});
		}

		function getId(data_imgs){
			$.ajax({
			    type: 'POST',
			    url: '../product/add_handler.php', // ссылка на товар
			    data: data_imgs, // 10891 - ID товара
			    success: function(id){
			    	postId(id);
			    }
			});
		}
	</script>

</body>
</html>

<?php else: ?>
	<script type="text/javascript">
		document.location.href="<?=$loginUrl?>";
	</script>
<?php endif;?>