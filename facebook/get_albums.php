<?

date_default_timezone_set('UTC');

require_once './config.php';

$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
$items=array();
$next=$_POST['next'];
$albums = $facebook->get("/me/albums?limit=12&fields=picture,name&after=".$next)->getGraphEdge();
$after=$albums->getMetaData();
$after=isset($after['paging']['next'])?$after['paging']['cursors']['after']:null;
foreach($albums as $album) {
	$album_preview = $album['picture']['url'];
	$item = array('id'=>$album['id'],
				'url' =>$album['picture']['url'], 
				'name'=>$album['name'],
			);
	array_push($items, $item);
}
echo json_encode(array('next'=>$after, 'items'=>$items));