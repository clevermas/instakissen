<?php

	define('UPLOAD_DIR', '../wp-content/uploads/upload_user/');

	$crp=new CroperPhp();

	$json='
	{"x": 393.6, "y": 96, "size_h":800, "size_h":800, "width": 1193.0, "offset_x": 0, "offset_y": 0, "url": "img.jpg"},
	{"x": 393.6, "y": 96, "size_h":800, "size_h":800, "width": 1193.0, "offset_x": 800, "offset_y": 0, "url": "img.jpg"},
	{"x": 393.6, "y": 96, "size_h":800, "size_h":800, "width": 1193.0, "offset_x": 0, "offset_y": 800, "url": "img.jpg"},
	{"x": 393.6, "y": 96, "size_h":800, "size_h":800, "width": 1193.0, "offset_x": 800, "offset_y": 800, "url": "img.jpg"}]';
	$json=$_POST["data"];
	//$json=json_decode($json,true);
	//echo json_last_error_msg();
	$uniq=uniqid();
	$source_file = $_POST["id"]."_".$uniq.".jpg";
	$thumb_file= $_POST["id"]."_".$uniq."_thumb.jpg";
	//echo ($json[0]["size_h"]);
	//echo json_encode($json);
	$crp->multiLoad($json);
	$crp->crop(4000);
	$source=$crp->save(UPLOAD_DIR.$source_file);
	$crp->getThumb(300);
	$thumb=$crp->save(UPLOAD_DIR.$thumb_file);
	$crp->dispose();

	echo json_encode(array('thumb'=>'upload_user/'.$thumb_file, 'source'=>'upload_user/'.$source_file));

	class CroperPhp{
		private $image;
		private $images=array();
		public function crop($s){
			$new_image = imagecreatetruecolor ($s, $s);
			foreach ($this->images as $key => $value) {
				$this->image=$value["image"];
				$tmp=$this->resizeToWidth($value["width"]);
				imagecopy($new_image, $tmp, $value["offset_x"], $value["offset_y"], $value["x"], $value["y"],$value["size_w"], $value["size_h"] );
			}
			$this->image=$new_image;
		}

		public function dispose(){
			imagedestroy($this->image);
		}

		public function getThumb($w){
			$this->image=$this->resizeToWidth($w);
		}

		public function multiLoad($arr){
			foreach ($arr as $key => $value) {
				//print $value["url"];
				$image=$this->load($value["url"]);
				$value["image"]=$image;
				array_push($this->images, $value);	 
			}
		}

		function save($filename, $compression=100, $permissions=null) {
			$res=false;
		    $res=imagejpeg($this->image,$filename,$compression);
		    return $res?$filename:null;
		}
		function load($filename) {
			$image=null;
      		$image_info = getimagesize($filename);
      		$image_type=$image_info[2];
      		if( $image_type == IMAGETYPE_JPEG ) {
         		$image = imagecreatefromjpeg($filename);
      		} elseif( $image_type == IMAGETYPE_GIF ) {
         		$image = imagecreatefromgif($filename);
      		} elseif( $image_type == IMAGETYPE_PNG ) {
         		$image = imagecreatefrompng($filename);
      		}
      		return $image;
  	 	}

		function getWidth() {
      		return imagesx($this->image);
   		}
   		
   		function getHeight() {
      		return imagesy($this->image);
   		}

		private function resizeToWidth($width) {
      		$ratio = $width / $this->getWidth();
      		$height = $this->getheight() * $ratio;
      		return $this->resize($width,$height);
   		}

   		private function resize($width,$height) {
      		$new_image = imagecreatetruecolor($width, $height);
      		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      		return $new_image;
   		}
		
		public function unzip($arch,$filename){
			$zip = new ZipArchive;
			$type=$this->image_type==IMAGETYPE_JPEG?'.jpg':($this->image_type==IMAGETYPE_GIF?'.gif':'.png');
			if ($zip->open($arch.'.zip', ZipArchive::CREATE) === TRUE) {
				$zip->addFile($filename.$type, $filename.$type);
				//unlink($filename.$type);
				$zip->close();
				return true;
			}
			//unlink($filename.$type);
			return false;
		}
	}
?>

