<?php

date_default_timezone_set('UTC');

require_once './config.php';

$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
$items=array();
$id_album = $_POST['id'];
$next=$_POST['next'];
$photos = $facebook->get("/$id_album/photos?limit=25&fields=id,images&after=".$next)->getGraphEdge();
$after=$photos->getMetaData();
$after=isset($after['paging']['next'])?$after['paging']['cursors']['after']:null;
foreach($photos as $photo) {
	$ind=count($photo['images'])/2-1;
	$ind=$ind<=0?0:$ind;
	$item = array('url' =>$photo['images'][0]['source'],
				'width'=> $photo['images'][0]['width'],
				'height'=> $photo['images'][0]['height'],
				'thumb'=>$photo['images'][$ind]['source'],
			);
	array_push($items, $item);
}
echo json_encode(array('next'=>$after, 'items'=>$items));