var ALBUMS_DATA={"albums":[], "next":''};
var ALBUMS_THUMB=[];
var uses_album={};
var size_algo=4000;
var no_used={
  "120":{
    "used":[
      [0,0,1],
      [0,0],
      [1,-1,0,0],
      [-1,-1,0,0],
      [0,0,1],
      [0,0]
    ],
    "size_w":4,
    "size_h":6,
  },
  "150":{
    "used":[
      [0,0,0,0],
      [0,1,-1,0],
      [0,-1,-1,0],
      [0,0,0,0],
    ],
    "size_w":4,
    "size_h":4,
  },
  "22":{
    used:[
      [0],
      [0]
    ],
    "size_w":1,
    "size_h":2,
  }
};

function albumsClickUpdate(){
	$('.lnk1').click( function() {
    	$('.tit1:eq(0)').html($(this).attr('name'));
      	if(uses_album[$(this).attr('id')]-1){
        	var html=renderPhotos(uses_album[$(this).attr('id')]-1, 0); 
        	$('#mCSB_1_container').html(html);
      		$('.content2').mCustomScrollbar("update");
      		$(".content2").mCustomScrollbar("scrollTo","top");
        	return;
      	}
      	ALBUMS_DATA['albums'].push({'id':$(this).attr('id'),'name':$(this).attr('name'),'next':null, 'images':[]});
      	var index=ALBUMS_DATA['albums'].length-1;
      	nextPhotos(ALBUMS_DATA['albums'].length-1, function(html){
      		$('#mCSB_1_container').html(html);
      		$('.content2').mCustomScrollbar("update");
        	console.log("next_photos");
      	});
      	uses_album[$(this).attr('id')]=index+1;
    });
}

var flag1=0,flag2;
function albumClick(){
	$('.clmn1 .content1 .lnk1').click(function(e) {
      e.preventDefault();
      flag2=$(this).index();
      if($(this).attr('dt')=='0') {
        if(flag1==0) {
          flag1=1;
          $(this).addClass('active').siblings().removeClass('active');
          $('.clmn2').animate({width:'177px'}, 500, 'easeInOutBack');
          $('.clmn2 .ov-clmn2').animate({left:'0px'}, 500, 'easeInOutBack');
        }
        else {
          $('.clmn1 .content1 .lnk1').attr('dt','0');
          //$('.clmn2').animate({width:'0px'}, 500);
          $('.clmn2 .ov-clmn2').animate({left:'-177px'}, 500, 'easeInOutBack');
          $('.clmn1 .content1 .lnk1').eq(flag2).addClass('active').siblings().removeClass('active');
          $('.clmn2').animate({width:'177px'}, 500, 'easeInOutBack');
          $('.clmn2 .ov-clmn2').animate({left:'0px'}, 500, 'easeInOutBack');
        }
        $(this).attr('dt','1');
      }
      else {
        $(this).removeClass('active');
        $('.clmn2').animate({width:'1px'}, 500, 'easeInOutBack');
        $('.clmn2 .ov-clmn2').animate({left:'-177px'}, 500, 'easeInOutBack');
        $(this).attr('dt','0');
        flag1=0;
      }
    });
}

$(document).ready(function() {
	$('body').css("visibility","visible");
	scrollPhotos();
	nextAlbums(function(html){
		$('.content1').append(html);
		scrollAlbums();
		albumClick();
		albumsClickUpdate();
	});
});

function func_plus(obj){
 obj=$(obj).parent().prev();
 var width=parseFloat($(obj).css("width")), height=parseFloat($(obj).css("height"));
 $(obj).css("width", width*1.05);
 $(obj).css("height", height*1.05);
}

function func_minus(obj){
 //console.log($(obj).parent().prev().parent());
 tmp=$(obj).parent().prev();
 var width=parseFloat($(tmp).css("width")), height=parseFloat($(tmp).css("height"));
 obj=$(tmp).parent();
 var obj_width=parseFloat($(obj).css("width")), obj_height=parseFloat($(obj).css("height"));
 if(obj_width>width/1.05||obj_height>height/1.05)return;
 $(tmp).css("width", width/1.05);
 $(tmp).css("height", height/1.05);

}

function func_del(obj){
 obj=$(obj).parent().parent();
 console.log(obj);
 $(obj).unbind();
 $(obj).html("");
}

function update(){
	$(".obj").droppable({
        accept: "div.div_img_rm",
        drop: function(b, c) {
          var attr=c.draggable[0].attributes;
        	var size_w=parseFloat($(this).css("width")), size_h=parseFloat($(this).css("height"));
        	var albums_index=attr[2].nodeValue, photos_index=attr[3].nodeValue;
        	var item = ALBUMS_DATA['albums'][albums_index]['images'][photos_index];

          var cover="";
          if(size_w>size_h){
            cover=size_w+"px";
            if(item.height*(size_w/item.width)<size_h)
               cover="auto "+size_h+"px";
          }
          else{
            cover="auto "+size_h+"px";
            if(item.width*(size_h/item.height)<size_w)
              cover=size_w+"px";
          }
          $(this).html("");
          $(this).unbind();
          $(this).attr("albums_index", albums_index);
          $(this).attr("photos_index", photos_index);
          $(this).css({"background":"url("+item.thumb+") no-repeat"});
          $(this).css({"background-size": cover});
          $(this).css({"background-position": "center center"});
          //$(this).backgroundDraggable();
          // console.log($(this).css("background-size"));
          // console.log($(this).css("background-position"));
        

        // $(this).hover( 
        //   function() {
        //     $(this).append('<div class="obj_func_edit" style="margin-top:10px; display:block; position:absolute"><div style="width:25px; height:25px; display:inline-block" id="func_plus" onclick="func_plus(this)" class="func_button img_div"></div><div id="func_minus" onclick="func_minus(this)" style="width:25px; height:25px; display:inline-block; margin-left:15px" class="func_button img_div"></div><div id="func_del" onclick="func_del(this)" style="width:25px; height:25px; margin-left:15px; display:inline-block" class="func_button img_div"></div></div>');
        //   }, 
        //   function() {
        //     $(this).find('.obj_func_edit').remove();
        //   }
        // );

        // $(this).css({
        //       background: "rgba(255, 255, 255, 0)",
        //         //border: "1px solid rgba(66, 66, 66, 0.8)",
				    //   border: "1px solid orange",
        //         "-webkit-background-clip": "padding-box",
        //         "background-clip": "padding-box"
        //     });
        },
    //     over: function() {
    //         $(this).css({
    //             background: "rgba(0, 0, 255, 0.4)",
    //             border: "1px solid rgba(0, 0, 255, 0.9)",
    //             "-webkit-background-clip": "padding-box",
    //             "background-clip": "padding-box"
    //         })
    //     },
    //     out: function() {
    //         $(this).css({
    //             background: "rgba(255, 255, 255, 0)",
    //             //border: "1px solid rgba(66, 66, 66, 0.8)",
				// border: "1px solid orange",
    //             "-webkit-background-clip": "padding-box",
    //             "background-clip": "padding-box"
    //         })
    //     }
    });
}

function selectedGridElementLi(th, ind){
  $('#main_object').html("");
  var size=parseInt($(th).attr("data-grid"));
  size=ind?ind:size;
  var k=0;
  if(!no_used[size])
    for(var i=0; i<size; i++)
      for(var j=0; j<size; j++){
        $('#main_object').append('<div class="obj ui-droppable" style="width: '+500/parseFloat(size)+'px; height: '+500/parseFloat(size)+'px; top: '+j*500/parseFloat(size)+'px; left: '+i*500/parseFloat(size)+'px; border: 1px solid orange; cursor: move; background: rgba(255, 255, 255, 0);"></div>');
      }
  else{
    var used=no_used[size].used;
    for(var i=0; i<used.length; i++)
      for(var j=0; j<used[i].length; j++){
        if(used[i][j]==0){
          $('#main_object').append('<div class="obj ui-droppable" style="width: '+500/no_used[size].size_w+'px; height: '+500/no_used[size].size_h+'px; top: '+i*500/no_used[size].size_h+'px; left: '+j*500/no_used[size].size_w+'px; border: 1px solid orange; cursor: move; background: rgba(255, 255, 255, 0);"></div>');
        }
        if(used[i][j]==1){
          $('#main_object').append('<div class="obj ui-droppable" style="width: '+(500/no_used[size].size_w)*2+'px; height: '+(500/no_used[size].size_h)*2+'px; top: '+i*500/no_used[size].size_h+'px; left: '+j*500/no_used[size].size_w+'px; border: 1px solid orange; cursor: move; background: rgba(255, 255, 255, 0);"></div>');
        }
      }
  }
  update();     
}

var load_photos=false, load_albums=false;

function scrollAlbums(){
	$(".content1").mCustomScrollbar({
		callbacks:{
			whileScrolling:function(){
				console.log(this.mcs.topPct);
				if(!load_albums&&ALBUMS_DATA['next']&&this.mcs.topPct>=95){
					load_albums=true;
					var tmp=this;
					nextAlbums(function (html){
                    	load_albums=false;
                    	tmp.mcs.content.append(html);
                    	albumsClickUpdate();
                    	albumClick();
                	});
				}
			},
			alwaysTriggerOffsets:false
		}
	});
}

function scrollPhotos(){
	$(".content2").mCustomScrollbar({
		callbacks:{
			whileScrolling:function(){
				console.log(this.mcs.topPct);
				if(!load_photos&&this.mcs.topPct>=95){
					var ind=uses_album[$('.active').attr('id')]-1;
					if(!ALBUMS_DATA['albums'][ind]['next'])return;
					load_photos=true;
					var tmp=this;
					nextPhotos(ind, function (html){
						load_photos=false;
						tmp.mcs.content.append(html);
						//drag_up_append();
					});
				}
			},
			onUpdate:function(){
      			console.log("Scrollbars updated");
      			drag_up_append();
    		},
			alwaysTriggerOffsets:false,
		}
	});
}

function nextAlbums(callback){
    $.ajax({
        type:'POST',
        url: 'get_albums.php',
        cache: false,
        data: {
            'next':ALBUMS_DATA['next']
        },
        success: function(response){
            response = eval("(" + response + ")");
            ALBUMS_DATA['next']=response['next'];
            var n=0;
            for(var i in response['items']){
                ALBUMS_THUMB.push(response['items'][i])
                n++;
            }
            var html=renderAlbums(ALBUMS_THUMB.length-n);
            if(callback)
                callback(html);
        }
    }); 
}

function renderAlbums(l){
    var html="";
    for(var i=l;i<ALBUMS_THUMB.length; i++)
        html+='<a href="" class="lnk1" dt="0" id="' +ALBUMS_THUMB[i]["id"] +'" name="' +ALBUMS_THUMB[i]["name"]+'"><img src="'+ALBUMS_THUMB[i]["url"]+'" alt=""><span>' +ALBUMS_THUMB[i]["name"]+ '</span></a>';
    return html;
}

function nextPhotos(index, callback){
	album=ALBUMS_DATA['albums'][index];
	id=album['id'];
	next=album['next']==null?'':album['next'];
	$.ajax({
     	type:'POST',
        url: 'get_photos.php',
        cache: false,
        data: {
        	'id':id,
      		'next':next
      	},
        success: function(response){
        	response = eval("(" + response + ")");
        	console.log(response);
			ALBUMS_DATA['albums'][index]['next']=response['next'];
			var n=0;
			for(var i in response['items']){
				ALBUMS_DATA['albums'][index]['images'].push(response['items'][i]);
				n++;
			}
			var html=renderPhotos(index,ALBUMS_DATA['albums'][index]['images'].length-n);
            if(callback)
            	callback(html);
        }
    });	
}

function renderPhotos(index, l){
	var images=ALBUMS_DATA['albums'][index]['images'];
	var html="";
	for(var i=l;i<images.length; i++)
		html+='<div class="backing"><div style="background-image:url('+images[i].thumb+');" class="div_img_rm ui-draggable" albums_index="'+index+'" photos_index="'+i+'"></div></div>';
	return html;
}	

function drag_up_append() {
    $("div.div_img_rm").draggable({
        helper: "clone",
        opacity: .9,
        cursor: "move",
        zIndex: 12,
        revert: 'invalid',
        appendTo: 'body'
    })
}