<?php
file_force_download($_GET['file'], $_GET['order_id']);
function file_force_download($file, $id) {
    if (ob_get_level()) {
      ob_end_clean();
    }
    header('Content-Disposition: attachment; filename='.$id.'_'.basename($file));
    ob_clean();
    flush();
    readfile($file);
   exit;
}
?>