<?php

date_default_timezone_set('UTC');

require_once './config.php';

$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
$items=array();
$id_album = $_POST['id'];
$next=$_POST['next'];
$photos = $facebook->get("/144880789197503/photos?limit=10&fields=id,images,paging".$next)->getGraphEdge();
echo $photos;
$after=isset($photos['paging']['next'])?$photos['paging']['cursors']['after']:null;
foreach($photos as $photo) {
	$ind=count($photo['images'])/2-1;
	$ind=$ind<=0?0:$ind;
	$item = array('url' =>$photo['images'][0]['source'], 
				'thumb'=>$photo['images'][$ind]['source'],
			);
	array_push($items, $item);
}
echo json_encode(array('next'=>$after, 'items'=>$items));