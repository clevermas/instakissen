<?php
	
	ini_set('memory_limit', '-1');
	
	function crop($file_input, $file_output, $crop = 'square',$percent = false) {
		list($w_i, $h_i, $type) = getimagesize($file_input);
		if (!$w_i || !$h_i) {
			echo 'Невозможно получить длину и ширину изображения';
			return;
		}
		$types = array('','gif','jpeg','png');
		$ext = $types[$type];
		if ($ext) {
			$func = 'imagecreatefrom'.$ext;
			$img = $func($file_input);
		} else {
			echo 'Некорректный формат файла';
			return;
		}
		
		if ($crop == 'square') {
			$min = $w_i;
			if ($w_i > $h_i) $min = $h_i;
			$w_o = $h_o = $min;
		} else {
			list($x_o, $y_o, $w_o, $h_o) = $crop;
			if ($percent) {
				$w_o *= $w_i / 100;
				$h_o *= $h_i / 100;
				$x_o *= $w_i / 100;
				$y_o *= $h_i / 100;
			}
					if ($w_o < 0) $w_o += $w_i;
				$w_o -= $x_o;
			if ($h_o < 0) $h_o += $h_i;
			$h_o -= $y_o;
		}
		$img_o = imagecreatetruecolor($w_o, $h_o);
		imagecopy($img_o, $img, 0, 0, $x_o, $y_o, $w_o, $h_o);
		if ($type == 2) {
			return imagejpeg($img_o,$file_output,100);
		} else {
			$func = 'image'.$ext;
			return $func($img_o,$file_output);
		}

		imagedestroy($img_o);
	}
	
	// requires php5
	define('UPLOAD_DIR', 'upload_user/');
	$img = $_POST['data'];
	$fb_id = $_POST['fb_id'];
	$img = str_replace('data:image/jpeg;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = UPLOAD_DIR.$fb_id."_".uniqid().'.jpg';
	$success = file_put_contents($file, $data);
	setcookie("userloadedfile","$file",time()+15552000);
	
	list($w_i, $h_i, $type) = getimagesize($file);
	crop($file,$file,array(16, 16, $w_i, $h_i));
	
	
// 	ini_set('memory_limit', '-1');
	
// 	function crop($file_input, $file_output, $crop = 'square',$percent = false) {
// 		list($w_i, $h_i, $type) = getimagesize($file_input);
// 		if (!$w_i || !$h_i) {
// 			echo 'Невозможно получить длину и ширину изображения';
// 			return;
// 		}
// 		$types = array('','gif','jpeg','png');
// 		$ext = $types[$type];
// 		if ($ext) {
// 			$func = 'imagecreatefrom'.$ext;
// 			$img = $func($file_input);
// 		} else {
// 			echo 'Некорректный формат файла';
// 			return;
// 		}
		
// 		if ($crop == 'square') {
// 			$min = $w_i;
// 			if ($w_i > $h_i) $min = $h_i;
// 			$w_o = $h_o = $min;
// 		} else {
// 			list($x_o, $y_o, $w_o, $h_o) = $crop;
// 			if ($percent) {
// 				$w_o *= $w_i / 100;
// 				$h_o *= $h_i / 100;
// 				$x_o *= $w_i / 100;
// 				$y_o *= $h_i / 100;
// 			}
// 					if ($w_o < 0) $w_o += $w_i;
// 				$w_o -= $x_o;
// 			if ($h_o < 0) $h_o += $h_i;
// 			$h_o -= $y_o;
// 		}
// 		$img_o = imagecreatetruecolor($w_o, $h_o);
// 		imagecopy($img_o, $img, 0, 0, $x_o, $y_o, $w_o, $h_o);
// 		if ($type == 2) {
// 			return imagejpeg($img_o,$file_output,100);
// 		} else {
// 			$func = 'image'.$ext;
// 			return $func($img_o,$file_output);
// 		}
// 	}
	
// 	// requires php5
// 	define('UPLOAD_DIR', 'upload_user/');
// 	$img = $_POST['data'];
// 	$fb_id = $_POST['fb_id'];
// 	$img = str_replace('data:image/jpeg;base64,', '', $img);
// 	$img = str_replace(' ', '+', $img);
// 	$data = base64_decode($img);
// 	$file = UPLOAD_DIR.$fb_id."_".uniqid().'.jpg';
// 	$success = file_put_contents($file, $data);
// 	setcookie("userloadedfile","$file",time()+15552000);
	
// 	list($w_i, $h_i, $type) = getimagesize($file);
// 	crop($file,$file,array(16, 16, $w_i, $h_i));
// 	


