<?php
ini_set('display_errors',1);

date_default_timezone_set('UTC');
require_once './config.php';
$helper = $facebook->getRedirectLoginHelper();

$loginUrl = $helper->getLoginUrl(
	'http://' .
		$_SERVER['HTTP_HOST'] . '/facebook/callback.php',
	['user_photos', 'user_friends']
);

 ?>

<?php if(isset($_SESSION['facebook_access_token'])): ?>
<?php
	
	//try {
	$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);

	// $req=$facebook->request('GET', '/me/albums');
	// $response = $facebook->getClient()->sendRequest($req);
	// $graphNode = $response->getGraphEdge();
	// echo $graphNode;

	$profile_img_get = $facebook->get('me/?fields=picture.type(large){url}')->getGraphObject();
	$profile_name_get = $facebook->get('me/')->getGraphObject();
	$profile_name = $profile_name_get['name'];
	$profile_id = $profile_name_get['id'];
	$profile_img_url = $profile_img_get['picture']['url'];

	//$session = new FacebookSession($_SESSION['facebook_access_token']);
	// $request = new Facebook\FacebookRequest($session,'GET',
 //  		'/me/albums'
	// );
	// $response = $request->execute();
	// $graphObject = $response->getGraphObject();

	// echo $graphObject;
	// $albums = $facebook->get('/me/albums')->getGraphEdge();
	// var_dump($photos);
	 /* foreach($albums as $album) {
			echo '<h1>' . $album['name'] . '</h1>';
			$photos = $facebook->get("/{$album['id']}/photos")->getGraphEdge();
			foreach($photos as $photo) {
				$photo = $facebook->get("/{$photo['id']}?fields=images")->getGraphObject();
				echo '<img src="' . $photo['images'][0]['source'] . '"><br>';
			}
		} */
		//$friendlist =
	 // var_dump($facebook->get('/me/friends')->getGraphEdge()); это было раскоментировано
	//} catch(Exception $exception) {
	//  echo '<i>Something went wrong</i>. Try <a href="' .
	//    htmlspecialchars($loginUrl) . '">signing in again</a>.';
	//}

	?>

	<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta charset="utf-8" />
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<title></title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<link rel="shortcut icon" href="favico.ico">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" type="text/css" media="screen, projection" />
		<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection" />
		<!--[if lte IE 6 ]><script type="text/javascript">window.location.href="ie6_close/index_ru.html";</script><![endif]-->
		<!--[if lt IE 9]> <link href= "css/ie8.css" rel= "stylesheet" media= "all" /> <![endif]-->

	<link rel="stylesheet" href="core-designer/nprogress.css">
	<link rel="stylesheet" href="core-designer/custom.css">

	<!-- Modals support -->
	<link rel="stylesheet" href="popups/css/bootstrap.css">
	<style>
	 .func_button{
		 cursor: default;
	 }

	 #func_plus{
		 background-repeat:no-repeat;
		 background-size:cover;
		 background-image: url("core-designer/img/controls-plus-01.png");
	 }
	 :hover#func_plus{
		 background-image: url("core-designer/img/controls-plus-01-hvr.png");
	 }

	 #func_minus{
		 background-repeat:no-repeat;
		 background-size:cover;
		 background-image: url("core-designer/img/controls-minus-01.png");
	 }
	 :hover#func_minus{
		 background-image: url("core-designer/img/controls-minus-01-hvr.png");
	 }

	 #func_del{
		 background-repeat:no-repeat;
		 background-size:cover;
		 background-image: url("core-designer/img/controls-del-01.png");
	 }
	 :hover#func_del{
		 background-image: url("core-designer/img/controls-del-01-hvr.png");
	 }

	 .next_load {
    background-color: #5cb85c;
    border: medium none;
    border-radius: 0 !important;
    color: white;
    font-size: 12px;
    font-weight: bold;
    padding: 10px;
    position: relative;
    text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3);
    width: 80%;
    margin-left: 20px;
	}


	</style>
</head>

<body style="visibility:hidden;">
	<img style="display:none;"  src="core-designer/img/controls-plus-01.png">
	<img style="display:none;"  src="core-designer/img/controls-minus-01.png">
	<img style="display:none;"  src="core-designer/img/controls-del-01.png">
	<img style="display:none;"  src="core-designer/img/controls-plus-01-hvr.png">
	<img style="display:none;"  src="core-designer/img/controls-minus-01-hvr.png">
	<img style="display:none;"  src="core-designer/img/controls-del-01-hvr.png">
	<header id="header">
				<a href="" class="logo"><img src="img/logo.png" alt=""></a>
				<div class="r-head">
						<div class="t1">Lieferung kostenlos</div>
						<div class="t2">Beratung<span>069-378-2928</span></div>
						<a href="" class="bt1">Kontakt</a>
						<a href="" class="bt2"></a>
				</div>
		</header>
		<section id="content">
				<div class="ll1">
						<div class="clmn1">
								<div class="av1">
										<div class="av-t1"><a href=""><div style="background-image:url('<?php print_r($profile_img_url); ?>');"></div><span><?=$profile_name?></span></a></div>
					<!--<div class="av-t1"><a href=""><img src="<?=$profile_img_url?>" alt=""><span><?=$profile_name?></span></a></div>-->
								</div>
								<div class="content1">
				<?php $albums = $facebook->get('/me/albums?fields=picture,name')->getGraphEdge();
				foreach($albums as $album) {
					$album_preview = $album['picture']['url'];
				 	echo '<a href="" class="lnk1" dt="0" id="' . $album['id'] . '" name="' . $album['name'] . '"><img src="' . $album_preview . '" alt=""><span>' . $album['name'] . '</span></a>';
				} ?>
					<button id="photos_next" class="next_load"><span class="load_span">Load more...</span></button>
								</div>
						</div>
						<div class="clmn2">
								<div class="ov-clmn2">
					<div id="getphoto"></div>
								</div>
						</div>
				</div>

		<!--<div id="designer" style="width:500px; height:500px; position:absolute; left:465px; top: 300px;"></div>

		<img id="background-image" src="img/podushka.png" style="position:absolute; left:465px; top:300px; width:503px; height:503px; z-index:-1999;"/>-->
		<div class="ll3">
						<div class="clmn3">
				<ul id="grid">
					<li data-name="1 grosen Bild" data-grid="1" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/1.png"></li>
					<li data-name="5 х 5 Bildern" data-grid="5" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/25.png"></li>
					<li data-name="4 х 4 Bildren" data-grid="4" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/16.png"></li>
					<li data-name="2 Bildern" data-grid="2" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/2.png"></li>
					<li data-name="2 х 2 Bildern" data-grid="2" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/4.png"></li>
					<li data-name="3 х 3 Bildern" data-grid="3" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/9.png"></li>
					<li data-name="12 Kleine Bildern und 1 grossen Bild" data-grid="150" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/13.png"></li>
					<li data-name="12 Kleine Bildern und 3 grossen Bildern" data-grid="120" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/15.png"></li>
				</ul>
			</div>
		</div>


		<div class="rr1">
						<div class="dc1">
								<div class="tit1" id="tit1" style="display:block;"><span id="quant_bild">2 х 2 Bildern</span>4 gleichen Kvadraten</div>

				<div id="designer" style="width:500px; height:500px; position:absolute; left:435px; top: 250px; display:block;">
					
					<div id="main_object" style="top: 0px; left: 0px; width: 500px; height: 500px;"></div>
					<div id="drag_photo_image"></div>
					<div id="main_object_load"></div>

				</div>
				<img id="background-image" src="img/podushka.png" style="position:absolute; left:435px; top:250px; width:503px; height:503px; z-index:-1999; display:block;"/>

				<div class="tit2" id="tit2" style="display:block;"><span>39,00€</span> 40 cm x 40 cm bedrucktes Kissen</div>
						</div>
				</div>


		<script type="text/javascript">
			function selectedGridElementLi(th){
				// $('#main_object').html("");
				// var size=parseInt($(th).attr("data-grid"));
				// for(var i=0; i<size; i++)
				// 	for(var j=0; j<size; j++)
				// 	$('#main_object').append('<div class="obj ui-droppable" style="width: '+500/parseFloat(size)+'px; height: '+500/parseFloat(size)+'px; top: '+j*500/parseFloat(size)+'px; left: '+i*500/parseFloat(size)+'px; border: 1px solid orange; cursor: move; background: rgba(255, 255, 255, 0);"></div>');
				document.getElementById("quant_bild").innerHTML = $(th).attr("data-name");
			}

			function save() {

				var a = document.getElementById("canvasrezult").toDataURL("image/jpeg", "1");
				console.log(a);
				$.ajax({
			      type: 'POST',
			      url: '../product/testkissen-3/', // ссылка на товар
			      data: 'add-to-cart=10891&quantity=1', // 10891 - ID товара
			      success: function(data){
				    // $('#docModal .modal-body').html(data);
			      }
			    });
				$.ajax({
					type: "POST",
					url: "save-designer.php",
					data: "data="+a+"&fb_id="+<?php print_r($profile_id); ?>,
					cache: !1,
					success: function() {
						console.log(this.data);
						NProgress.done();
						// document.location.href = "cart.php";
						document.location.href = "/cart.php";
					},
					error: function() {
						console.log('Ajax error:');
						console.dir(arguments);
					}
				});
			}
		</script>

		
	</section>

	<div class="ft-bot1"><a href="#" id="save_design">weiter</a></div>
	

	<!-- Modal -->
	<div class="modal fade" id="modal1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
					<div class="loading-icon"></div>
				</div>
				<div class="modal-body">
					<h4 class="modal-title">Deine kissen ist fast fertig...</h4>
					<p>Deine Kissen würde jetzt gespeichert!</p>
				</div>
				<div class="modal-footer">
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- Modal end -->

	<!-- Modal -->
    <div class="modal fade" id="modal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-image: url(popups/img/modal2-header.jpg);">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title">Wie willst du die Bilder hochladen?</h4>
                </div>
                <div class="modal-footer">
                    <div class="buttons-wrapper">
                        <a href="#" class="modal-btn facebook"></a>
                        <a href="#" class="modal-btn instagram"></a>
                        <a href="#" class="modal-btn drive">einfach Bilder von Festplatte hinzufügen</a>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal end -->


	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="core-designer/jqueryui.min.js"></script>
	<script src="core-designer/js/jquery.ui.touch-punch.js"></script>
	<script src="core-designer/upload/js/plupload.full.min.js"></script>
	<script src="core-designer/boot3.js"></script>
	<script src="core-designer/bootstrap.min.js"></script>
	<script src="core-designer/nprogress.js"></script>


	<!--<script src="js/jquery.js"></script>-->
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
	<script type="text/javascript" src="js/common.js"></script>

</body>
</html>

<?php else: ?>
	<a href="<?=htmlspecialchars($loginUrl)?>">Sign in with Facebook!</a>;
<?php endif;?>
