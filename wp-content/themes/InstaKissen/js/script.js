'use strict';

// Header navigation

(function($)
{

	$(function()
	{

		// products in basket

		function getCookie(c_name)
		{
		    var i,x,y,ARRcookies=document.cookie.split(";");

		    for (i=0;i<ARRcookies.length;i++)
		    {
		        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		        x=x.replace(/^\s+|\s+$/g,"");
		        if (x==c_name)
		        {
		            return unescape(y);
		        }
		     }
		}
		
		var count = parseInt(getCookie('products_in_basket'));
		(count)&&($('.cart-top .basket').html(count).addClass('in'));
	});



})(jQuery);


// Modals

(function($)
{
	$(function()
	{
		$('.gallery01 .et_pb_button').on('click', function(e)
		{
			e.preventDefault();
			$('#modal2').modal('show');
		})
	});
})(jQuery);


// Columns equalization

(function($)
{
	$(function()
	{
		$(window).on('resize', function()
		{
			$('#map01 .et_pb_map').height($('#gallery01').height());
		});

		setTimeout(function()
		{
			$(window).resize();
		}, 300);
	});
})(jQuery);


// Script Sync Load

var scriptSyncLoad, firstLoad = true, scriptSyncLoadCheckers = [];

// check if storage is not empty
if (typeof localStorage.getItem('scriptSyncLoad') != 'undefined')
localStorage.removeItem('scriptSyncLoad');	  

(function($)
{
scriptSyncLoad = function()
{

	var deleteStorage;
	        
	$('.script-sync-loader').each(function()
	{
	  var $t = $(this),
	      cover = $t.find('[data-src]'),
	      dataSrc = cover.data('src'),
	      coverImg = $('<img>'),
	      img = $t.find('img'),
	      isCoverLoaded, isImgLoaded, checkerInterval;
	      
	  var scriptSyncLoadStorage = JSON.parse(localStorage.getItem('scriptSyncLoad'));
	      
	  if (scriptSyncLoadStorage && scriptSyncLoadStorage.src.indexOf(dataSrc) != -1 && !firstLoad)
	  {
	  cover.css('background-image', 'url(' + dataSrc + ')');
	  $t.addClass('ready');
	  deleteStorage = true;
	  return
	  }
	  
	     
	  coverImg.on('load', function()
	  {
	    cover.css('background-image', 'url(' + this.src + ')');
	    coverImg.remove();
	    isCoverLoaded = true;
	  }).attr('src', dataSrc);
	  
	  img.on('load', function()
	  {
	  	isImgLoaded = true;
	  });
	  
	  var checkerIndex = scriptSyncLoadCheckers.length;
	  
	  
	   scriptSyncLoadCheckers.push( setInterval(function()
		{
			if (isCoverLoaded && isImgLoaded)
			{
			  $t.addClass('ready');
			  
			  if (!scriptSyncLoadStorage)
			  {
			    scriptSyncLoadStorage = {src:[]};
			    localStorage.setItem('scriptSyncLoad',  JSON.stringify(scriptSyncLoadStorage));
			  }
			      
			  scriptSyncLoadStorage.src.push(dataSrc);
			  localStorage.setItem('scriptSyncLoad', JSON.stringify(scriptSyncLoadStorage));
			  
			  firstLoad = false;
			  
			  clearInterval(scriptSyncLoadCheckers[checkerIndex]);
			  delete scriptSyncLoadCheckers[checkerIndex];
			}
		}, 500) );
		
	});
	
	(deleteStorage)&&(localStorage.removeItem('scriptSyncLoad'));
	


}

	$(scriptSyncLoad);
})(jQuery);


// WooCommerce

(function($)
{
	$(function()
	{
		var valueChecker = function()
		{
			var $frow = $(this).closest('.form-row');
			if (this.value)
				$frow.addClass('form-row-filled');
			else
				$frow.removeClass('form-row-filled')
		};

		$('.woocommerce').each(function()
		{
			var $wc = $(this);

			var controlsFormatting = function()
			{
				$wc // Radio buttons
					.find('.radio input, .checkbox input').each(function()
					{
						$('<span class="riple"></span><span class="circle"></span><span class="check"></span>').insertAfter(this);
					}).end();
			};

			$wc	// Form Controls
			    .find('.woocommerce-billing-fields .form-row:first').addClass('form-row--first').end()

			    // Focus/unfocus
				.find('.input-text').focus(function()
				{
					$(this).closest('.form-row').addClass('form-row-focus');
				}).blur(function()
				{
					$(this).closest('.form-row').removeClass('form-row-focus');
				}).on('change', valueChecker).each(valueChecker)
				.end();

			controlsFormatting();

			// WooCommerce hooks
			$('body')
				.on('updated_checkout', controlsFormatting)
				.on('updated_checkout', scriptSyncLoad)
				.on('updated_shipping_method', controlsFormatting)
				.on('country_to_state_changing', function()
			{
				// Get rid of formatting of postcode field
				setTimeout(function(){$('#billing_postcode_field').removeClass('form-row-first').find('input').attr('placeholder', '');},100);
			});
		});
	});
})(jQuery)