var browserSync = require('browser-sync').create();
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var prefix = require('gulp-autoprefixer');
var bsconfig = require('./bs-config.js');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('default', ['server'], function()
{
  gulp.watch(['./scss/**/*.scss', './bower_components/cleverk-divi-sass/scss/**/*.scss'], function(event)
  {
    gulp.run('sass');
  });
});


// SASS

gulp.task('sass', function ()
{
  
  return gulp.src('./scss/style.scss')

    // .pipe(sourcemaps.init())

    .pipe(sass(
    {
    	outputStyle:'expanded'
    	// sourceComments: 'map'
    }).on('error', sass.logError))

    // .pipe(sourcemaps.write())

     // Pass the compiled sass through the prefixer with defined 
    .pipe(prefix(
        'last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'
    ))

    .pipe(gulp.dest('./'));
    // .pipe(browserSync.stream());
});


// Server

gulp.task('server', function()
{
  // browserSync.init(bsconfig);
 // browserSync.init({
 //      proxy   : "http://localhost/instakissen",
 //      files: ['sass/**/*.scss', '../../**/*.php', '../../../plugins/woocommerce/**/*.php']
 //  });
});
