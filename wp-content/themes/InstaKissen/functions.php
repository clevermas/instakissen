<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

        
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

// END ENQUEUE PARENT ACTION

// Include jQuery Select Menu

function clever_selectmenu() {
    wp_enqueue_script( 'jquery-ui-selectmenu', array('jquery') );
}

add_action( 'wp_enqueue_scripts', 'clever_selectmenu' );

function clever_selectmenu_init() {
    echo "<script>jQuery('.wpcf7-select').selectmenu();</script>";
}

add_action( 'wp_footer', 'clever_selectmenu_init', 100 );

function clever_theme_styles() {
	wp_enqueue_style( 'selectmenu', get_stylesheet_directory_uri() . '/css/selectmenu.css' );
}

add_action( 'wp_enqueue_scripts', 'clever_theme_styles' );


// include script.js

function clever_theme_script() {
    wp_enqueue_script( 'theme-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), true );
}

add_action( 'wp_enqueue_scripts', 'clever_theme_script' );

function clever_pad_editor_modal() {
    ?>
    <!-- Modal -->
    <div class="modal fade" id="modal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-image: url(facebook/popups/img/modal2-header.jpg);">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title">Wie willst du die Bilder hochladen?</h4>
                </div>
                <div class="modal-footer">
                    <div class="buttons-wrapper">
                        <a href="/facebook" class="modal-btn facebook"></a>
                        <a href="#" class="modal-btn instagram"></a>
                        <a href="#" class="modal-btn drive">einfach Bilder von Festplatte hinzuf���gen</a>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal end -->
    <?php
}

add_action( 'wp_footer', 'clever_pad_editor_modal' );

function clever_pad_editor_popups_css()
{
    wp_enqueue_style( 'pad-editor-modals', '/facebook/popups/css/bootstrap.css', '', '1.0.0' );
    wp_enqueue_script( 'pad-editor-modals', '/facebook/core-designer/bootstrap.min.js', array('jquery'), '', '', true);
}

add_action( 'wp_enqueue_scripts', 'clever_pad_editor_popups_css' );

add_filter('et_pb_gallery_image_height', 'clever_fix_mainpage_gallery_image_height');

function clever_fix_mainpage_gallery_image_height($height)
{
    return is_front_page() ? 400 : $height;
}

/* WooCommerce Customization */

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Hook off
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields )
{
	// Billing fields

     $billing_fields = 
     	array(
	     	'billing_first_name',
	     	'billing_last_name',
	     	'billing_postcode',
	     	'billing_address_1',
	     	'billing_company',
	     	'billing_email',
	     	'billing_phone',
            'billing_country'
     	);

     $billing = array();

     foreach ($billing_fields as $name)
     {

     	$billing[$name] = $fields['billing'][$name];

     	$field = &$billing[$name];

     	unset($field['class']);
     	unset($field['placeholder']);

     	switch ($name)
     	{
     		case 'billing_first_name':
     			$field['label'] = 'Vorname';
     			break;

     		case 'billing_last_name':
     			$field['label'] = 'Nachname';
     			break;

     		case 'billing_postcode':
     			$field['label'] = 'Postleitzahl';
     			break;

			case 'billing_address_1':
     			$field['label'] = 'Stra&szlig;e und Hausnummer';
     			break;


     		case 'billing_company':
     			$field['label'] = 'Firma (optional)';
     			break;

     		case 'billing_email':
     			$field['label'] = 'E-Mail-Adresse';
     			break;

     		case 'billing_phone':
     			$field['label'] = 'Telefonnummer';
     			break;

     	}
     	
     }

     $fields['billing'] = $billing;

     unset($fields['order']['order_comments']); 

     return $fields;
}



/**
Add the email notification option field to the checkout
*/

add_action('woocommerce_review_order_before_submit', 'email_notifications_checkout_field');

function email_notifications_checkout_field()
{
    ?>
    <div class="checkbox">
        <label>
            <input type="checkbox" class="input-checkbox" name="email_notifications" id="email_notifications">
            Informiert mich ���ber Neuigkeiten und Angebote.
        </label>
    </div>
    <?php
}


/**
Update the order meta with field value
*/
add_action('woocommerce_checkout_update_order_meta', 'email_notifications_update_order_meta');

function email_notifications_update_order_meta( $order_id ) {
    if ($_POST['email_notifications']) update_post_meta( $order_id, 'email_notifications', esc_attr($_POST['email_notifications']));
}

/**
 Display field value on the order edition page
*/
add_action( 'woocommerce_admin_order_data_after_billing_address', 'email_notifications_display_admin_order_meta', 10, 1 );

function email_notifications_display_admin_order_meta($order)
{
    // echo '<p><strong>'.__('Email Notifications').':</strong> ' . ($order->order_custom_fields['email_notifications'] ? 'On' : 'Off') . '</p>';
}



/**
 Thank you text 
*/
add_action('woocommerce_thankyou_order_received_text', 'clever_thankyou_text');

function clever_thankyou_text($text)
{
    return 'Vielen Dank f&uuml;r Ihre Bestellung';
}