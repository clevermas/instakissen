<?php
/** Enable W3 Total Cache */
define('WP_CACHE', false); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'instakissen');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n/2D#P {ySwRTkVJ|-}[x2gT5}{V_UIQ5%Y<6>hiuv~u> t3Kbu|&D[Q~qU%aquT');
define('SECURE_AUTH_KEY',  'fn;plv3gSi`Y[j-s}(^>k2-vWv{6bmI=WVr3JMyYIayMF-yr$$c%c|,tB%1rd46$');
define('LOGGED_IN_KEY',    '2?}xFw5-M/.uIjF^K|A+gtjuBKNF*pp/;p|*|H^KNQ,rp>e{8gYCN$Sm[<K-+cU+');
define('NONCE_KEY',        ';-j||e/?KqdZEldG&Q$D/?s#L}^i?@>03uA2<@_%]Sv~|zWfF<!o-|B)Qm]e89.W');
define('AUTH_SALT',        '42ZP5=_fy(]to,4aH..7%r?0B[biQG%;*v%|jVRE59>/h>G+mBT! J.fQ3X>CTj+');
define('SECURE_AUTH_SALT', 'u!V9JfQ[<&F%@JAx)tt@^m`-}|Hh[UECbBo+NLbQT;&Qbhs67N&_Z4-kbv_yb/f1');
define('LOGGED_IN_SALT',   '+8OE:EGV;4^z-+FN<fU~<j*|k^/w|grk{`y VwS0kGvr:jfC}R]Xl{nkKUD#d-%~');
define('NONCE_SALT',       '_OqYL+!T (9|ERLPQ8-.o><MnGDo|F%k-u<s>;>IEuhW_}f)5QZ&bgI<gg&nN/},');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');