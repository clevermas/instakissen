<?php
date_default_timezone_set('UTC');

require_once './config.php';

$helper = $facebook->getRedirectLoginHelper();
$_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
header('Location: /samples/facebook');