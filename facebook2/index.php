<?php
ini_set('display_errors',1);

date_default_timezone_set('UTC');
require_once './config.php';
$helper = $facebook->getRedirectLoginHelper();

$loginUrl = $helper->getLoginUrl(
  'http://' .
    $_SERVER['HTTP_HOST'] . '/facebook/callback.php',
  ['user_photos', 'user_friends']
);

 ?>

<?php if(isset($_SESSION['facebook_access_token'])): ?>
<?php

  //try {
	$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
	$profile_img_get = $facebook->get('me/?fields=picture.type(large){url}')->getGraphObject();
	$profile_name_get = $facebook->get('me/')->getGraphObject();
	$profile_name = $profile_name_get['name'];
	$profile_id = $profile_name_get['id'];
	$profile_img_url = $profile_img_get['picture']['url'];
	// $albums = $facebook->get('/me/albums')->getGraphEdge();
	// var_dump($photos);
   /* foreach($albums as $album) {
      echo '<h1>' . $album['name'] . '</h1>';
      $photos = $facebook->get("/{$album['id']}/photos")->getGraphEdge();
      foreach($photos as $photo) {
        $photo = $facebook->get("/{$photo['id']}?fields=images")->getGraphObject();
        echo '<img src="' . $photo['images'][0]['source'] . '"><br>';
      }
    } */
    //$friendlist = 
   // var_dump($facebook->get('/me/friends')->getGraphEdge()); это было раскоментировано
  //} catch(Exception $exception) {
  //  echo '<i>Something went wrong</i>. Try <a href="' .
  //    htmlspecialchars($loginUrl) . '">signing in again</a>.';
  //}
  
  ?>
  
  <!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection" />
    <!--[if lte IE 6 ]><script type="text/javascript">window.location.href="ie6_close/index_ru.html";</script><![endif]-->
    <!--[if lt IE 9]> <link href= "css/ie8.css" rel= "stylesheet" media= "all" /> <![endif]-->
	
	<link rel="stylesheet" href="core-designer/custom.css">
	<style>
	 .func_button{
		 cursor: default;
	 }	 
	 
	 #func_plus{
		 background-repeat:no-repeat;
		 background-size:cover;
		 background-image: url("core-designer/img/controls-plus-01.png");
	 }
	 :hover#func_plus{
		 background-image: url("core-designer/img/controls-plus-01-hvr.png");
	 }
	 
	 #func_minus{
		 background-repeat:no-repeat;
		 background-size:cover;
		 background-image: url("core-designer/img/controls-minus-01.png");
	 }
	 :hover#func_minus{
		 background-image: url("core-designer/img/controls-minus-01-hvr.png");
	 }
	 
	 #func_del{
		 background-repeat:no-repeat;
		 background-size:cover;
		 background-image: url("core-designer/img/controls-del-01.png");
	 }
	 :hover#func_del{
		 background-image: url("core-designer/img/controls-del-01-hvr.png");
	 }
	</style>
</head>

<body>
	<img style="display:none;"  src="core-designer/img/controls-plus-01.png">
	<img style="display:none;"  src="core-designer/img/controls-minus-01.png">
	<img style="display:none;"  src="core-designer/img/controls-del-01.png">
	<img style="display:none;"  src="core-designer/img/controls-plus-01-hvr.png">
	<img style="display:none;"  src="core-designer/img/controls-minus-01-hvr.png">
	<img style="display:none;"  src="core-designer/img/controls-del-01-hvr.png">
	<header id="header">
        <a href="" class="logo"><img src="img/logo.png" alt=""></a>
        <div class="r-head">
            <div class="t1">Lieferung kostenlos</div>
            <div class="t2">Beratung<span>069-378-2928</span></div>
            <a href="" class="bt1">Kontakt</a>
            <a href="" class="bt2"></a>
        </div>
    </header>
    <section id="content">
        <div class="ll1">
            <div class="clmn1">
                <div class="av1">
                    <div class="av-t1"><a href=""><div style="background-image:url('<?php print_r($profile_img_url); ?>');"></div><span><?=$profile_name?></span></a></div>
					<!--<div class="av-t1"><a href=""><img src="<?=$profile_img_url?>" alt=""><span><?=$profile_name?></span></a></div>-->
                </div>
                <div class="content1">
				<?php $albums = $facebook->get('/me/albums')->getGraphEdge();
				foreach($albums as $album) {
				$album_preview_get_id = $facebook->get("/{$album['id']}?fields=cover_photo")->getGraphObject();
				$album_preview_get = $facebook->get("/{$album_preview_get_id['cover_photo']['id']}?fields=picture")->getGraphObject();
				$album_preview = $album_preview_get['picture'];
				
				 echo '<a href="" class="lnk1" dt="0" id="' . $album['id'] . '" name="' . $album['name'] . '"><img src="' . $album_preview . '" alt=""><span>' . $album['name'] . '</span></a>';
				 //!/echo '<div class="div_img_rm ui-draggable" data-drag-img="'.$album_preview.'"><a href="#" class="lnk1" dt="0" id="' . $album['id'] . '" name="' . $album['name'] . '"><img class="img_rm" src="' . $album_preview . '" alt=""><span>' . $album['name'] . '</span></a></div>';
				} ?>
                </div>
            </div>
            <div class="clmn2">
                <div class="ov-clmn2">
					<div id="getphoto"></div>
                </div>
            </div>
        </div>
		
		<!--<div id="designer" style="width:500px; height:500px; position:absolute; left:465px; top: 300px;"></div>
		
		<img id="background-image" src="img/podushka.png" style="position:absolute; left:465px; top:300px; width:503px; height:503px; z-index:-1999;"/>-->

		<div class="ll3">
            <div class="clmn3">			
				<ul id="grid">
					<li data-name="1 grosen Bild" data-grid="1" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/1.png"></li>
					<li data-name="5 х 5 Bildern" data-grid="25" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/25.png"></li>
					<li data-name="4 х 4 Bildren" data-grid="16" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/16.png"></li>
					<li data-name="2 Bildern" data-grid="2" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/2.png"></li>
					<li data-name="2 х 2 Bildern" data-grid="4" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/4.png"></li>
					<li data-name="3 х 3 Bildern" data-grid="9" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/9.png"></li>					
					<li data-name="12 Kleine Bildern und 1 grossen Bild" data-grid="150" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/13.png"></li>
					<li data-name="12 Kleine Bildern und 3 grossen Bildern" data-grid="120" onclick="selectedGridElementLi(this);"><img src="img/pod_grid2/15.png"></li>
				</ul>
			</div>
		</div>
	
	
		<div class="rr1">
            <div class="dc1">
                <div class="tit1" id="tit1" style="display:none;"><span id="quant_bild">2 х 2 Bildern</span>16 gleichen Kvadraten</div>
					
				<div id="designer" style="width:500px; height:500px; position:absolute; left:435px; top: 250px; display:none;"></div>
				<img id="background-image" src="img/podushka.png" style="position:absolute; left:435px; top:250px; width:503px; height:503px; z-index:-1999; display:none;"/>
				
			  <div class="tit2" id="tit2" style="display:none;"><span>39,00€</span> 40 cm x 40 cm bedrucktes Kissen</div>
            </div>
        </div>
		
		
		<script type="text/javascript">
			function selectedGridElementLi(th){
				document.getElementById("quant_bild").innerHTML = $(th).attr("data-name");
			}
			
			function save() {
				var a = document.getElementById("canvasrezult").toDataURL("image/jpeg", "1");
				console.log(a);
				$.ajax({
					type: "POST",
					url: "save-designer.php",
					data: "data="+a+"&fb_id="+<?php print_r($profile_id); ?>,
					cache: !1,
					success: function() {
						document.location.href = "cart.php"
					}
				})
			}
		</script>
		
	</section>
	
	<div class="ft-bot1"><a href="#" id="save_design">weiter</a></div>
	
	
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="core-designer/jqueryui.min.js"></script>
	<script src="core-designer/js/jquery.ui.touch-punch.js"></script>
	<script src="core-designer/upload/js/plupload.full.min.js"></script>
	<script src="core-designer/boot3.js"></script>
	
	
	<!--<script src="js/jquery.js"></script>-->
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	
</body>
</html>
  
<?php else: ?>
  <a href="<?=htmlspecialchars($loginUrl)?>">Sign in with Facebook!</a>;
<?php endif;?>
