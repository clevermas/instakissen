$(document).ready(function() {
	
	var pCenter = 216+((document.body.clientWidth-216-89)/2)-250;
	document.getElementById("designer").style.left = pCenter+"px";
	document.getElementById("designer").style.display = "block";
	document.getElementById("background-image").style.left = pCenter+"px";
	document.getElementById("background-image").style.display = "block";
	document.getElementById("tit1").style.left = pCenter+"px";
	document.getElementById("tit1").style.display = "block";
	document.getElementById("tit2").style.left = pCenter+"px";
	document.getElementById("tit2").style.display = "block";
	
	$('input, textarea').placeholder();
	/* PIE */
	if (window.PIE) {
		$('nav').each(function() {
			PIE.attach(this);
		});
	}
	var ff;
	$('input[type=text]').focus(function() {
		if($(this).attr('data-place')==$(this).val()) {
			$(this).val('');
		}
	});
	$('input[type=text]').blur(function() {
		if($(this).val()=='') {
			$(this).val($(this).attr('data-place'));
		}
	});
	$('textarea').focus(function() {
		if($(this).attr('data-place')==$(this).val()) {
			$(this).val('');
		}
	});
	$('textarea').blur(function() {
		ff=$(this).attr('data-place');
		if($(this).val().length==0) {
			$(this).val(ff);
		}
	});
	function ress() {
		$('.clmn1,.clmn2').height($(window).height()-73-86);
		$('.ov-clmn2').height($('.clmn2').height());
		//$('.dc1').width($('.rr1').width()).height($('.clmn1').height());
		$('.clmn1 .content1').height($('.clmn2').height()-($(".av1").height()+11));
		$('.clmn2 .content1').height($('.clmn2').height()-(100+32));
	}
	$(window).load(function() {
		$('input[type=text]').each(function() {
			$(this).attr('data-place',$(this).val());
		});
		$('textarea').each(function() {
			$(this).attr('data-place',$(this).val());
		});
		ress();
		
		prepareScroll();
		
		funck();
	});
	$(window).resize(function() {
		ress();
	});
	var flag1=0;
	var flag2;
	$('.clmn1 .content1 .lnk1').click(function(e) {
		e.preventDefault();
		flag2=$(this).index();
		if($(this).attr('dt')=='0') {
			if(flag1==0) {
				flag1=1;
				$(this).addClass('active').siblings().removeClass('active');
				$('.clmn2').animate({width:'195px'}, 500, 'easeInOutBack');
				$('.clmn2 .ov-clmn2').animate({left:'0px'}, 500, 'easeInOutBack');
			}
			else {
				$('.clmn1 .content1 .lnk1').attr('dt','0');
				//$('.clmn2').animate({width:'0px'}, 500);
				$('.clmn2 .ov-clmn2').animate({left:'-195px'}, 500, 'easeInOutBack');
				setTimeout(function() {
					$('.clmn1 .content1 .lnk1').eq(flag2).addClass('active').siblings().removeClass('active');
					$('.clmn2').animate({width:'195px'}, 500, 'easeInOutBack');
					$('.clmn2 .ov-clmn2').animate({left:'0px'}, 500, 'easeInOutBack');
				}, 600);
			}
			$(this).attr('dt','1');
		}
		else {
			$(this).removeClass('active');
			$('.clmn2').animate({width:'1px'}, 500, 'easeInOutBack');
			$('.clmn2 .ov-clmn2').animate({left:'-195px'}, 500, 'easeInOutBack');
			$(this).attr('dt','0');
			flag1=0;
		}
	});
	function funck() {
		var et=0;
		$('.ov-clmn2 .content1 img').each(function() {
			$(this).attr('id',et);
			et++;
		});
	}
});
function allowDrop(ev) {
	ev.preventDefault();
}
function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);
}
function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	ev.target.appendChild(document.getElementById(data));
}

function prepareScroll(){
	$(".content1").mCustomScrollbar({
		horizontalScroll:false,
		mouseWheelPixels: 300
	});
}

function prepareScroll2(){
	$(".content2").mCustomScrollbar({
		horizontalScroll:false,
		mouseWheelPixels: 300
	});
}




































$('.lnk1').click( function() {
	$.ajax({
	  type: 'POST',
	  url: 'get_photos.php',
	  data: 'id_album=' + $(this).attr('id') + '&name_album=' + $(this).attr('name'),
	  success: function(data){
		$('#getphoto').html(data);
		drag_up_append();
		prepareScroll2();
	  }
	});
});

function raport_device() {
    $.ajax({
        type: "POST",
        url: "app/raport-device.php",
        cache: !1
    })
}

function getPlatform() {
    for (var a in userDeviceArray) userDeviceArray[a].platform.test(platform) && (raport_device(), alert("\u041e\u0431\u0440\u0430\u0442\u0438\u0442\u0435 \u0432\u043d\u0438\u043c\u0430\u043d\u0438\u0435! \n\r \n\r\u041c\u044b \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u043c \u043d\u0430\u0434 \u0442\u0435\u043c, \u0447\u0442\u043e\u0431\u044b \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u043f\u043e\u0434\u0443\u0448\u0435\u043a \u0441 \u043c\u043e\u0431\u0438\u043b\u044c\u043d\u044b\u0445 \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432 \u0438 \u043f\u043b\u0430\u043d\u0448\u0435\u0442\u043e\u0432 \u0441\u0442\u0430\u043b\u043e \u0432\u043e\u0437\u043c\u043e\u0436\u043d\u044b\u043c. \u0421\u0435\u0439\u0447\u0430\u0441 \u0432\u0430\u0448 \u0437\u0430\u043a\u0430\u0437 \u043d\u0435  \u0441\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c\u0441\u044f ;(\n\r \n\r\u041f\u0440\u043e\u0441\u0438\u043c \u0437\u0430\u0439\u0442\u0438 \u0441 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u0430 \u0438\u043b\u0438 \u043d\u043e\u0443\u0442\u0431\u0443\u043a\u0430 \u0438 \u043e\u0444\u043e\u0440\u043c\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437."));
    return "\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043f\u043b\u0430\u0442\u0444\u043e\u0440\u043c\u0430!" +
    platform
}
getPlatform();

function drop_draw_image(a) {
    object_offset = $("#obj_" + a).offset();
    offsetX[a] = object_offset.left;
    offsetY[a] = object_offset.top;
    X[a] = 0;
    Y[a] = 0;
    1 == koef_canvas && !1 === flag_ef && (sX[a] = 0, sY[a] = 0);
	obj_image[a] = new Image;
	obj_image[a].crossOrigin = "anonymous";
    obj_image[a].onload = function() {
        1 == koef_canvas && !1 === flag_ef ? (setTimeout(function() {
			//alert(1);
            obj_image[a].width > obj_image[a].height / obj_height[a] * obj_width[a] ? (left_offset[a] = (obj_image[a].width - obj_image[a].height / obj_height[a] * obj_width[a]) / 2, top_offset[a] = 0, img_width[a] = obj_image[a].height / obj_height[a] *
            obj_width[a], img_height[a] = obj_image[a].height) : (left_offset[a] = 0, top_offset[a] = (obj_image[a].height - obj_image[a].width / obj_width[a] * obj_height[a]) / 2, img_width[a] = obj_image[a].width, img_height[a] = obj_image[a].width / obj_width[a] * obj_height[a])
        }, 0), setTimeout(function() {
            design_ctx.drawImage(obj_image[a], left_offset[a], top_offset[a], img_width[a], img_height[a], obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a])
        }, 0)) : !0 === flag_ef ? setTimeout(function() {
			//alert(3);
            design_ctx.drawImage(obj_image[a], left_offset[a] -
            sX[a], top_offset[a] - sY[a], img_width[a], img_height[a], obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a])
        }, 0) : setTimeout(function() {
			//alert(4);
            rezult_ctx.drawImage(obj_image[a], left_offset[a] - sX[a], top_offset[a] - sY[a], img_width[a], img_height[a], obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a])
        }, 0)
    };
    obj_image[a].src = img_src[a]
}

function object_events(a) {
	
    $("#obj_" + a).hover(function() {
            "" !== img_src[a] && !1 === flag_load ? ($("#obj_" + a).html('<div class="obj_func_edit" style="margin-top:10px;"><div style="z-index:3009999;" id="func_plus" class="func_button img_div"></div><div id="func_minus" class="func_button img_div"></div><div id="func_del" class="func_button img_div"></div></div>'), $(".func_button").css({
                "z-index": "100999",
                display: "inline-block"
            }), "150" < obj_width[a] / koef_canvas ? ($("#func_edit").css({
                "z-index": "1009999",
                margin: "0px 10px"
            }), $("#func_minus").css({
                "z-index": "1009999",
                margin: "0px 10px"
            }), $("#func_plus").css({
                "z-index": "1009999",
                margin: "0px 10px"
            }), $("#func_ok").css({
                "z-index": "1009999",
                margin: "0px 10px"
            }), $("#func_del").css({
                "z-index": "1009999",
                margin: "0px 10px"
            }), $(".obj_func_edit").css({
                "z-index": "1009999",
                "margin-top": "-10",
                "margin-left": "-90"
            //}), $(".obj_func_edit img").css({
			}), $(".obj_func_edit .img_div").css({
                "z-index": "1009999",
                width: "25",
				height: "25"
            }),
                $(".obj_func_start").css({
                    "z-index": "1009999",
                    "margin-top": "10",
                    "margin-left": "-50"
                }), $(".obj_func_start img").css({
                "z-index": "1009999",
                width: "30"
            })) : ($("#func_edit").css({
                "z-index": "1099990",
                margin: "5px"
            }), $("#func_minus").css({
                "z-index": "1009999",
                margin: "5px"
            }), $("#func_plus").css({
                "z-index": "1009999",
                margin: "5px"
            }), $("#func_ok").css({
                "z-index": "1009999",
                margin: "5px"
            }), $("#func_del").css({
                "z-index": "1009999",
                margin: "5px"
            }), $(".obj_func_edit").css({
                "margin-top": "1",
                "margin-left": "-55"
            //}), $(".obj_func_edit img").css({
			}), $(".obj_func_edit .img_div").css({
                width: "20",
				height: "20"
            }),
                $(".obj_func_start").css({
                    "margin-top": "1",
                    "margin-left": "-30"
                }), $(".obj_func_start img").css({
                width: "20"
            })), "70" > obj_width[a] / koef_canvas && ($("#func_edit").css({
                "z-index": "1009999",
                margin: "2px"
            }), $("#func_minus").css({
                "z-index": "1099990",
                margin: "2px"
            }), $("#func_plus").css({
                "z-index": "1009999",
                margin: "2px"
            }), $("#func_ok").css({
                "z-index": "1009999",
                margin: "2px"
            }), $("#func_del").css({
                "z-index": "1009999",
                margin: "2px"
            }), $(".obj_func_edit").css({
                "margin-top": "-120",
                "margin-left": "-33"
            //}), $(".obj_func_edit img").css({
			}), $(".obj_func_edit .img_div").css({
                width: "16",
				height: "16"
            }),
                $(".obj_func_start").css({
                    "margin-top": "-4",
                    "margin-left": "-30"
                }), $(".obj_func_start img").css({
                width: "16"
            })), $("#func_edit").bind("click", function() {
                $(".obj_func_start").hide();
                $(".obj_func_edit").show();
                $("#obj_" + a).css({
                    cursor: "move"
                })
            }), $("#func_ok").bind("click", function() {
                $(".obj_func_start").show();
                $(".obj_func_edit").hide();
                $("#obj_" + a).css({
                    cursor: "default"
                })
            }), $("#func_del").bind("click", function() {
                design_ctx.clearRect(obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a]);
                img_src[a] =
                    "";
                obj_image[a].src = "";
                $("#obj_" + a).html('<a class="href-message" ></a>')
            }), $("#func_plus").bind("click", function() {
                design_ctx.clearRect(obj_left_offset[a],
                    obj_top_offset[a], obj_width[a], obj_height[a]);
                img_width[a] *= koef_zoom_plus;
                img_height[a] *= koef_zoom_plus;
                left_offset[a] += img_width[a] * (1 - koef_zoom_plus) / 2;
                top_offset[a] += img_height[a] * (1 - koef_zoom_plus) / 2;
                design_ctx.drawImage(obj_image[a], left_offset[a] - sX[a], top_offset[a] - sY[a], img_width[a], img_height[a], obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a])
            }), $("#func_minus").bind("click", function() {
                left_offset[a] += img_width[a] * (1 - koef_zoom_minus) / 2;
                top_offset[a] += img_height[a] * (1 - koef_zoom_minus) /
                2;
                img_width[a] *= koef_zoom_minus;
                img_height[a] *= koef_zoom_minus;
                0 > top_offset[a] && (top_offset[a] = 0);
                0 > left_offset[a] && (left_offset[a] = 0);
                sX[a] >= left_offset[a] && (sX[a] = left_offset[a]);
                sX[a] <= -left_offset[a] && (sX[a] = -left_offset[a]);
                sY[a] >= top_offset[a] && (sY[a] = top_offset[a]);
                sY[a] <= -top_offset[a] && (sY[a] = -top_offset[a]);
                img_height[a] <= obj_image[a].height && img_width[a] <= obj_image[a].width ? (design_ctx.clearRect(obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a]), design_ctx.drawImage(obj_image[a],
                    left_offset[a] - sX[a], top_offset[a] - sY[a], img_width[a], img_height[a], obj_left_offset[a], obj_top_offset[a], obj_width[a], obj_height[a])) : obj_image[a].width > obj_image[a].height / obj_height[a] * obj_width[a] ? (left_offset[a] = (obj_image[a].width - obj_image[a].height / obj_height[a] * obj_width[a]) / 2, top_offset[a] = 0, img_width[a] = obj_image[a].height / obj_height[a] * obj_width[a], img_height[a] = obj_image[a].height) : (left_offset[a] = 0, top_offset[a] = (obj_image[a].height - obj_image[a].width / obj_width[a] * obj_height[a]) / 2, img_width[a] =
                    obj_image[a].width, img_height[a] = obj_image[a].width / obj_width[a] * obj_height[a])
            })) : !1 === flag_load && $("#obj_" + a).html('<a class="href-message" ></a>')
        },
        function() {
            !1 === flag_load && $("#obj_" + a).html("")
        });
    $("#obj_" + a).css({
        background: "rgba(255, 255, 255, 0)",
        //border: "1px solid #666"
		border: "1px solid orange"
    });
    $("#obj_" + a).mousedown(function(b) {
        handleMouseDown(b, a)
    });
    $("#obj_" + a).mousemove(function(b) {
        handleMouseMove(b, a)
    });
    $("#obj_" + a).mouseup(function(b) {
        handleMouseUp(b, a)
    });
    $("#obj_" + a).mouseout(function(b) {
        handleMouseOut(b, a)
    });
    $("#obj_" + a).droppable({
        accept: "div.div_img_rm",
        drop: function(b, c) {
            c.draggable.attr("data-social") ? (flag_load = !0, $("#obj_" +
            a).html('<div class="loding_div_now"><img class="loding_img_now" src="core-designer/img/ajax-loader-2.gif" /></div>'), $.ajax({
                type: "POST",
                url: "core-designer/ajax_copy_img.php",
                data: "img=" + c.draggable.attr("data-drag-img"),
                cache: !1,
                success: function(b) {
                    img_src[a] = b;
                    drop_draw_image(a);
                    $("#obj_" + a).html("");
                    flag_load = !1
                }
            })) : (flag_load = !0, img_src[a] = c.draggable.attr("data-drag-img"), drop_draw_image(a), flag_load = !1);
            $(this).css({
                background: "rgba(255, 255, 255, 0)",
                //border: "1px solid rgba(66, 66, 66, 0.8)",
				border: "1px solid orange",
                "-webkit-background-clip": "padding-box",
                "background-clip": "padding-box"
            })
        },
        over: function() {
            $(this).css({
                background: "rgba(0, 0, 255, 0.4)",
                border: "1px solid rgba(0, 0, 255, 0.9)",
                "-webkit-background-clip": "padding-box",
                "background-clip": "padding-box"
            })
        },
        out: function() {
            $(this).css({
                background: "rgba(255, 255, 255, 0)",
                //border: "1px solid rgba(66, 66, 66, 0.8)",
				border: "1px solid orange",
                "-webkit-background-clip": "padding-box",
                "background-clip": "padding-box"
            })
        }
    })
}

function first_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
	
    $("#main_object").html("");
    $("#main_object").append('<div id="obj_1" class="obj"></div>');
    $("#obj_1").css({
        width: w_device / koef_canvas - 2,
        height: h_device / koef_canvas - 2
    });
    obj_width[1] = w_device;
    obj_height[1] = h_device;
    obj_left_offset[1] = 0;
    obj_top_offset[1] = 0;
    object_events(1)
}

function two_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    $("#main_object").append('<div id="obj_1" class="obj"></div><div id="obj_2" class="obj"></div>');
    $("#obj_1").css({
        width: w_device / koef_canvas - 2,
        height: h_device / koef_canvas / 2 - 2
    });
    $("#obj_2").css({
        width: w_device / koef_canvas - 2,
        height: h_device / koef_canvas / 2 - 2,
        top: h_device / koef_canvas / 2
    });
    obj_width[1] = w_device;
    obj_height[1] = h_device / 2;
    obj_left_offset[1] = 0;
    obj_top_offset[1] = 0;
    object_events(1);
    obj_width[2] =
        w_device;
    obj_height[2] = h_device / 2;
    obj_left_offset[2] = 0;
    obj_top_offset[2] = h_device / 2;
    object_events(2)
}

function three_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    $("#main_object").append('<div id="obj_1" class="obj"></div><div id="obj_2" class="obj"></div><div id="obj_3" class="obj"></div>');
    $("#obj_1").css({
        width: w_device / koef_canvas - 2,
        height: h_device / koef_canvas / 3 - 2
    });
    $("#obj_2").css({
        width: w_device / koef_canvas - 2,
        height: h_device / koef_canvas / 3 - 2,
        top: h_device / koef_canvas / 3
    });
    $("#obj_3").css({
        width: w_device / koef_canvas - 2,
        height: h_device / koef_canvas /
        3 - 2,
        top: h_device / koef_canvas / 3 * 2
    });
    obj_width[1] = w_device;
    obj_height[1] = h_device / 3;
    obj_left_offset[1] = 0;
    obj_top_offset[1] = 0;
    object_events(1);
    obj_width[2] = w_device;
    obj_height[2] = h_device / 3;
    obj_left_offset[2] = 0;
    obj_top_offset[2] = h_device / 3;
    object_events(2);
    obj_width[3] = w_device;
    obj_height[3] = h_device / 3;
    obj_left_offset[3] = 0;
    obj_top_offset[3] = h_device / 3 * 2;
    object_events(3)
}

function two_horizontal_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 3 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 2,
        b = (h_device / koef_canvas - 2);
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: "0",
        left: 2 * a
    });
    $("#obj_4").css({
        top: "0",
        left: 3 * a
    });
    $("#obj_5").css({
        top: b,
        left: "0"
    });
    $("#obj_6").css({
        top: b,
        left: a
    });
    $("#obj_7").css({
        top: b,
        left: 2 * a
    });
    $("#obj_8").css({
        top: b,
        left: 3 * a
    });
    $("#obj_9").css({
        top: 2 * b,
        left: "0"
    });
    $("#obj_10").css({
        top: 2 * b,
        left: a
    });
    $("#obj_11").css({
        top: 2 * b,
        left: 2 * a
    });
    $("#obj_12").css({
        top: 2 * b,
        left: 3 * a
    });
    $("#obj_13").css({
        top: 3 * b,
        left: "0"
    });
    $("#obj_14").css({
        top: 3 * b,
        left: a
    });
    $("#obj_15").css({
        top: 3 * b,
        left: 2 * a
    });
    $("#obj_16").css({
        top: 3 * b,
        left: 3 * a
    });
    for (a = 1; 3 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px", "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px",
        "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 * koef_canvas, obj_top_offset[a] = Number($("#obj_" + a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] += 2 * koef_canvas, object_events(a)
}

function eight_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 9 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / 2 - 2) / koef_canvas,
        b = (h_device / 4 - 2) / koef_canvas;
    $("#obj_1").css({
        width: a,
        height: b,
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        width: a,
        height: b,
        left: a + 2,
        top: "0"
    });
    $("#obj_3").css({
        width: a,
        height: b,
        top: b + 2,
        left: "0"
    });
    $("#obj_4").css({
        width: a,
        height: b,
        top: b + 2,
        left: a + 2
    });
    $("#obj_5").css({
        width: a,
        height: b,
        top: 2 * b + 4,
        left: "0"
    });
    $("#obj_6").css({
        width: a,
        height: b,
        top: 2 * b + 4,
        left: a + 2
    });
    $("#obj_7").css({
        width: a,
        height: b,
        top: 3 * b + 6,
        left: "0"
    });
    $("#obj_8").css({
        width: a,
        height: b,
        top: 3 * b + 6,
        left: a + 2
    });
    for (a = 1; 9 > a; a++) obj_width[a] = Math.round(Number($("#obj_" + a).css("width").replace("px", ""))) * koef_canvas + 2 * koef_canvas, obj_height[a] = Math.round(Number($("#obj_" + a).css("height").replace("px", ""))) * koef_canvas + 2 * koef_canvas, obj_left_offset[a] = Math.round(Number($("#obj_" + a).css("left").replace("px", ""))) * koef_canvas,
        obj_top_offset[a] = Math.round(Number($("#obj_" + a).css("top").replace("px", ""))) * koef_canvas, object_events(a)
}

function fifteen_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 16 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / 3 - 2) / koef_canvas,
        b = (h_device / 5 - 2) / koef_canvas;
    $("#obj_1").css({
        width: a,
        height: b,
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        width: a,
        height: b,
        top: "0",
        left: a + 2
    });
    $("#obj_3").css({
        width: a,
        height: b,
        top: "0",
        left: 2 * (a + 2)
    });
    $("#obj_4").css({
        width: a,
        height: b,
        top: b + 2,
        left: "0"
    });
    $("#obj_5").css({
        width: a,
        height: b,
        top: b + 2,
        left: a + 2
    });
    $("#obj_6").css({
        width: a,
        height: b,
        top: b + 2,
        left: 2 * (a + 2)
    });
    $("#obj_7").css({
        width: a,
        height: b,
        top: 2 * (b + 2),
        left: "0"
    });
    $("#obj_8").css({
        width: a,
        height: b,
        top: 2 * (b + 2),
        left: a + 2
    });
    $("#obj_9").css({
        width: a,
        height: b,
        top: 2 * (b + 2),
        left: 2 * (a + 2)
    });
    $("#obj_10").css({
        width: a,
        height: b,
        top: 3 * (b + 2),
        left: "0"
    });
    $("#obj_11").css({
        width: a,
        height: b,
        top: 3 * (b + 2),
        left: a + 2
    });
    $("#obj_12").css({
        width: a,
        height: b,
        top: 3 * (b + 2),
        left: 2 * (a + 2)
    });
    $("#obj_13").css({
        width: a,
        height: b,
        top: 4 * (b + 2),
        left: "0"
    });
    $("#obj_14").css({
        width: a,
        height: b,
        top: 4 * (b + 2),
        left: a + 2
    });
    $("#obj_15").css({
        width: a,
        height: b,
        top: 4 * (b + 2),
        left: 2 * (a + 2)
    });
    for (a = 1; 16 > a; a++) obj_width[a] = Math.round(Number($("#obj_" + a).css("width").replace("px", ""))) * koef_canvas + 2 * koef_canvas, obj_height[a] = Math.round(Number($("#obj_" + a).css("height").replace("px", ""))) * koef_canvas + 2 * koef_canvas, obj_left_offset[a] = Math.round(Number($("#obj_" + a).css("left").replace("px", ""))) * koef_canvas, obj_top_offset[a] = Math.round(Number($("#obj_" + a).css("top").replace("px", ""))) * koef_canvas,
        object_events(a)
}

function twenty_four_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 25 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 4,
        b = (h_device / koef_canvas - 2) / 6;
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: "0",
        left: 2 * a
    });
    $("#obj_4").css({
        top: "0",
        left: 3 * a
    });
    $("#obj_5").css({
        top: b,
        left: "0"
    });
    $("#obj_6").css({
        top: b,
        left: a
    });
    $("#obj_7").css({
        top: b,
        left: 2 * a
    });
    $("#obj_8").css({
        top: b,
        left: 3 * a
    });
    $("#obj_9").css({
        top: 2 * b,
        left: "0"
    });
    $("#obj_10").css({
        top: 2 * b,
        left: a
    });
    $("#obj_11").css({
        top: 2 * b,
        left: 2 * a
    });
    $("#obj_12").css({
        top: 2 * b,
        left: 3 * a
    });
    $("#obj_13").css({
        top: 3 * b,
        left: "0"
    });
    $("#obj_14").css({
        top: 3 * b,
        left: a
    });
    $("#obj_15").css({
        top: 3 * b,
        left: 2 * a
    });
    $("#obj_16").css({
        top: 3 * b,
        left: 3 * a
    });
    $("#obj_17").css({
        top: 4 * b,
        left: "0"
    });
    $("#obj_18").css({
        top: 4 * b,
        left: a
    });
    $("#obj_19").css({
        top: 4 * b,
        left: 2 * a
    });
    $("#obj_20").css({
        top: 4 * b,
        left: 3 *
        a
    });
    $("#obj_21").css({
        top: 5 * b,
        left: "0"
    });
    $("#obj_22").css({
        top: 5 * b,
        left: a
    });
    $("#obj_23").css({
        top: 5 * b,
        left: 2 * a
    });
    $("#obj_24").css({
        top: 5 * b,
        left: 3 * a
    });
    for (a = 1; 25 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px", "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 * koef_canvas, obj_top_offset[a] = Number($("#obj_" +
    a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] += 2 * koef_canvas, object_events(a)
}

function four_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 5 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 2,
        b = (h_device / koef_canvas - 2) / 2;
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: b,
        left: "0"
    });
    $("#obj_4").css({
        top: b,
        left: a
    });
    for (a = 1; 5 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px",
        "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 * koef_canvas, obj_top_offset[a] = Number($("#obj_" + a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] += 2 * koef_canvas, object_events(a)
}

function nine_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 10 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 3,
        b = (h_device / koef_canvas - 2) / 3;
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: "0",
        left: 2 * a
    });
    $("#obj_4").css({
        top: b,
        left: "0"
    });
    $("#obj_5").css({
        top: b,
        left: a
    });
    $("#obj_6").css({
        top: b,
        left: 2 * a
    });
    $("#obj_7").css({
        top: 2 *
        b,
        left: "0"
    });
    $("#obj_8").css({
        top: 2 * b,
        left: a
    });
    $("#obj_9").css({
        top: 2 * b,
        left: 2 * a
    });
    for (a = 1; 10 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px", "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 * koef_canvas, obj_top_offset[a] = Number($("#obj_" + a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] +=
        2 * koef_canvas, object_events(a)
}

function sixteen_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 17 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 4,
        b = (h_device / koef_canvas - 2) / 4;
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: "0",
        left: 2 * a
    });
    $("#obj_4").css({
        top: "0",
        left: 3 * a
    });
    $("#obj_5").css({
        top: b,
        left: "0"
    });
    $("#obj_6").css({
        top: b,
        left: a
    });
    $("#obj_7").css({
        top: b,
        left: 2 * a
    });
    $("#obj_8").css({
        top: b,
        left: 3 * a
    });
    $("#obj_9").css({
        top: 2 * b,
        left: "0"
    });
    $("#obj_10").css({
        top: 2 * b,
        left: a
    });
    $("#obj_11").css({
        top: 2 * b,
        left: 2 * a
    });
    $("#obj_12").css({
        top: 2 * b,
        left: 3 * a
    });
    $("#obj_13").css({
        top: 3 * b,
        left: "0"
    });
    $("#obj_14").css({
        top: 3 * b,
        left: a
    });
    $("#obj_15").css({
        top: 3 * b,
        left: 2 * a
    });
    $("#obj_16").css({
        top: 3 * b,
        left: 3 * a
    });
    for (a = 1; 17 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px", "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px",
        "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 * koef_canvas, obj_top_offset[a] = Number($("#obj_" + a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] += 2 * koef_canvas, object_events(a)
}

function twenty_five_squares_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 26 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 5,
        b = (h_device / koef_canvas - 2) / 5;
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: "0",
        left: 2 * a
    });
    $("#obj_4").css({
        top: "0",
        left: 3 * a
    });
    $("#obj_5").css({
        top: "0",
        left: 4 * a
    });
    $("#obj_6").css({
        top: b,
        left: "0"
    });
    $("#obj_7").css({
        top: b,
        left: a
    });
    $("#obj_8").css({
        top: b,
        left: 2 * a
    });
    $("#obj_9").css({
        top: b,
        left: 3 * a
    });
    $("#obj_10").css({
        top: b,
        left: 4 * a
    });
    $("#obj_11").css({
        top: 2 * b,
        left: "0"
    });
    $("#obj_12").css({
        top: 2 * b,
        left: a
    });
    $("#obj_13").css({
        top: 2 * b,
        left: 2 * a
    });
    $("#obj_14").css({
        top: 2 * b,
        left: 3 * a
    });
    $("#obj_15").css({
        top: 2 * b,
        left: 4 * a
    });
    $("#obj_16").css({
        top: 3 * b,
        left: "0"
    });
    $("#obj_17").css({
        top: 3 * b,
        left: a
    });
    $("#obj_18").css({
        top: 3 * b,
        left: 2 * a
    });
    $("#obj_19").css({
        top: 3 * b,
        left: 3 * a
    });
    $("#obj_20").css({
        top: 3 * b,
        left: 4 * a
    });
    $("#obj_21").css({
        top: 4 * b,
        left: "0"
    });
    $("#obj_22").css({
        top: 4 * b,
        left: a
    });
    $("#obj_23").css({
        top: 4 * b,
        left: 2 * a
    });
    $("#obj_24").css({
        top: 4 * b,
        left: 3 * a
    });
    $("#obj_25").css({
        top: 4 * b,
        left: 4 * a
    });
    for (a = 1; 26 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px", "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 *
    koef_canvas, obj_top_offset[a] = Number($("#obj_" + a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] += 2 * koef_canvas, object_events(a)
}

function different_squares_grid() {
	1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 16 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
	//var a = w_device / koef_canvas / 4,
	var a = 125,
        b = 2 * a,
        //c = h_device / koef_canvas / 6,
		c = 83,
        d = 2 * c;
	//alert(a);
	$("#obj_1").css({
        width: a,
        height: c,
        top: "0",
        left: "0"
    });
	$("#obj_2").css({
        width: a,
        height: c,
        top: "0",
        left: a
    });
	$("#obj_3").css({
        width: a,
        height: c,
        top: c,
        left: "0"
    });
	$("#obj_4").css({
        width: a,
        height: c,
        top: c,
        left: a
    });
	$("#obj_5").css({
        //width: b-2,
		width: b,
        height: d,
        top: "0",
        left: 2 * a
    });
	$("#obj_6").css({
        width: b,
        height: d,
        top: 2 * c,
        left: "0"
    });
	$("#obj_7").css({
        width: a,
        height: c,
        top: d,
        left: b
    });
    $("#obj_8").css({
        //width: a-2,
		width: a,
        height: c,
        top: d,
        left: b + a
    });
    $("#obj_9").css({
        width: a,
        height: c,
        top: d + c,
        left: b
    });
    $("#obj_10").css({
        //width: a-2,
		width: a,
        height: c,
        top: d + c,
        left: b + a
    });
    $("#obj_11").css({
        width: a,
        height: c,
        top: d + 2 * c,
        left: "0"
    });
	$("#obj_12").css({
        width: a,
        height: c,
        top: d + 2 * c,
        left: a
    });
	$("#obj_13").css({
        width: a,
        //height: c-2,
		height: c,
        top: d + 3 * c,
        left: "0"
    });
	$("#obj_14").css({
        width: a,
        //height: c-2,
		height: c,
        top: d + 3 * c,
        left: a
    });
	$("#obj_15").css({
        //width: b-2,
		width: b,
        //height: d-2,
		height: d,
        top: d + 2 * c,
        left: 2 * a
    });
    for (a = 1; 16 > a; a++){
		obj_width[a] = Math.round(Number($("#obj_" + a).css("width").replace("px", ""))) * koef_canvas;
		obj_height[a] = Math.round(Number($("#obj_" + a).css("height").replace("px", ""))) * koef_canvas;
		obj_left_offset[a] = 2+Math.round(Number($("#obj_" + a).css("left").replace("px", ""))) * koef_canvas;
		obj_top_offset[a] = 2+Math.round(Number($("#obj_" + a).css("top").replace("px", ""))) * koef_canvas;
		object_events(a);
	}
}

function magic_grid() {
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 9 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    $("#obj_1").css({
        width: (3 * w_device / 4 - 2) / koef_canvas,
        height: (h_device / 3 - 2) / koef_canvas,
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        width: (w_device / 4 - 2) / koef_canvas,
        height: (h_device / 6 - 2) / koef_canvas,
        top: "0",
        left: (3 * w_device / 4 - 2) / koef_canvas + 2
    });
    $("#obj_3").css({
        width: (w_device / 4 - 2) / koef_canvas,
        height: (h_device / 6 - 2) / koef_canvas,
        top: (h_device / 6 - 2) / koef_canvas + 2,
        left: (3 * w_device / 4 - 2) / koef_canvas + 2
    });
    $("#obj_4").css({
        width: (w_device / 2 - 2) / koef_canvas,
        height: (h_device / 3 - 2) / koef_canvas,
        top: (h_device / 3 - 2) / koef_canvas + 2,
        left: "0"
    });
    $("#obj_5").css({
        width: (w_device / 2 - 2) / koef_canvas,
        height: (h_device / 3 - 2) / koef_canvas,
        top: (h_device / 3 - 2) / koef_canvas + 2,
        left: (w_device / 2 - 2) / koef_canvas + 2
    });
    $("#obj_6").css({
        width: (w_device / 4 - 2) / koef_canvas,
        height: (h_device / 6 - 2) / koef_canvas,
        top: 2 * ((h_device / 3 - 2) / koef_canvas + 2),
        left: "0"
    });
    $("#obj_7").css({
        width: (w_device /
        4 - 2) / koef_canvas,
        height: (h_device / 6 - 2) / koef_canvas,
        top: (5 * h_device / 6 - 2) / koef_canvas + 2,
        left: "0"
    });
    $("#obj_8").css({
        width: (3 * w_device / 4 - 2) / koef_canvas,
        height: (h_device / 3 - 2) / koef_canvas,
        top: 2 * ((h_device / 3 - 2) / koef_canvas + 2),
        left: (w_device / 4 - 2) / koef_canvas + 2
    });
    for (a = 1; 9 > a; a++) obj_width[a] = Math.round(Number($("#obj_" + a).css("width").replace("px", ""))) * koef_canvas + 2 * koef_canvas, obj_height[a] = Math.round(Number($("#obj_" + a).css("height").replace("px", ""))) * koef_canvas + 2 * koef_canvas, obj_left_offset[a] = Math.round(Number($("#obj_" +
    a).css("left").replace("px", ""))) * koef_canvas, obj_top_offset[a] = Math.round(Number($("#obj_" + a).css("top").replace("px", ""))) * koef_canvas, object_events(a)
}

function new_sixteen_squares_grid() {
	//almsit_6
    1 == koef_canvas && design_ctx.clearRect(0, 0, w_device, h_device);
    $("#main_object").html("");
    for (var a = 1; 17 > a; a++) $("#main_object").append('<div id="obj_' + a + '" class="obj">');
    var a = (w_device / koef_canvas - 2) / 4,
        b = (h_device / koef_canvas - 2) / 4;
    $(".obj").css({
        width: a,
        height: b
    });
    $("#obj_1").css({
        top: "0",
        left: "0"
    });
    $("#obj_2").css({
        top: "0",
        left: a
    });
    $("#obj_3").css({
        top: "0",
        left: 2 * a
    });
    $("#obj_4").css({
        top: "0",
        left: 3 * a
    });
    $("#obj_5").css({
        top: b,
        left: "0"
    });
    //TODO:
    $("#obj_6").css({
        top: b,
        left: a,
        width: a * 2,
        height: b * 2
    });
    /*$("#obj_6").css({
        top: b,
        left: a
    });*/
	
	
    $("#obj_7").css({
        top: 0,
        left: 0,
		width: 0,
        height: 0
    });
	
    $("#obj_8").css({
        top: b,
        left: 3 * a
    });
    $("#obj_9").css({
        top: 2 * b,
        left: "0"
    });
   
	$("#obj_10").css({
        top: 0,
        left: 0,
		width: 0,
        height: 0
    });
	$("#obj_11").css({
        top: 0,
        left: 0,
		width: 0,
        height: 0
    });
   
    $("#obj_12").css({
        top: 2 * b,
        left: 3 * a
    });
    $("#obj_13").css({
        top: 3 * b,
        left: "0"
    });
    $("#obj_14").css({
        top: 3 * b,
        left: a
    });
    $("#obj_15").css({
        top: 3 * b,
        left: 2 * a
    });
    $("#obj_16").css({
        top: 3 * b,
        left: 3 * a
    });
    for (a = 1; 17 > a; a++) obj_width[a] = Number($("#obj_" + a).css("width").replace("px", "")).toFixed(0) * koef_canvas, obj_height[a] = Number($("#obj_" + a).css("height").replace("px",
        "")).toFixed(0) * koef_canvas, obj_left_offset[a] = Number($("#obj_" + a).css("left").replace("px", "")).toFixed(0) * koef_canvas, obj_left_offset[a] += 2 * koef_canvas, obj_top_offset[a] = Number($("#obj_" + a).css("top").replace("px", "")).toFixed(0) * koef_canvas, obj_top_offset[a] += 2 * koef_canvas, object_events(a)
}

function formation_object(a) {
    switch (a) {
        case "1":
            first_grid();
            drop_draw_image(1);
            grid_count = 1;
            break;
        case "2":
            two_grid();
            for (a = 1; 3 > a; a++) drop_draw_image(a);
            grid_count = 2;
            break;
        case "20":
            two_horizontal_grid();
            for (a = 1; 3 > a; a++) drop_draw_image(a);
            grid_count = 2;
            break;
        case "3":
            three_grid();
            for (a = 1; 4 > a; a++) drop_draw_image(a);
            grid_count = 3;
            break;
        case "8":
            eight_squares_grid();
            for (a = 1; 9 > a; a++) drop_draw_image(a);
            grid_count = 8;
            break;
        case "15":
            fifteen_squares_grid();
            for (a = 1; 16 > a; a++) drop_draw_image(a);
            grid_count = 15;
            break;
        case "24":
            twenty_four_squares_grid();
            for (a = 1; 25 > a; a++) drop_draw_image(a);
            grid_count =
                24;
            break;
        case "150":
			//almsit_5
            new_sixteen_squares_grid();
            for (a = 1; 17 > a; a++) drop_draw_image(a);
            grid_count = 16;
            break;
        case "120":
            different_squares_grid();
            for (a = 1; 16 > a; a++) drop_draw_image(a);
            grid_count = 15;
            break;
        case "80":
            magic_grid();
            for (a = 1; 9 > a; a++) drop_draw_image(a);
            grid_count = 8;
            break;
        case "4":
            four_squares_grid();
            for (a = 1; 5 > a; a++) drop_draw_image(a);
            grid_count = 4;
            break;
        case "9":
            nine_squares_grid();
            for (a = 1; 10 > a; a++) drop_draw_image(a);
            grid_count = 9;
            break;
        case "16":
            sixteen_squares_grid();
            for (a = 1; 17 > a; a++) drop_draw_image(a);
            grid_count = 16;
            break;
        case "25":
            twenty_five_squares_grid();
            for (a = 1; 26 > a; a++) drop_draw_image(a);
            grid_count = 25
    }
}

function async(a) {
    if (a < grid_count + 1) {
        var b = $("div.div_img_rm").size();
        0 == b && alert("\u0412\u044b \u0435\u0449\u0435 \u043d\u0435 \u0437\u0430\u0433\u0440\u0443\u0437\u0438\u043b\u0438 \u043d\u0438 \u043e\u0434\u043d\u043e\u0439 \u0444\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438.");
        b = Math.floor(Math.random() * b);
        $("div.div_img_rm").eq(b).attr("data-social") ? $.ajax({
            type: "POST",
            url: "core-designer/ajax_copy_img.php",
            data: "img=" + $("div.div_img_rm").eq(b).attr("data-drag-img"),
            cache: !1,
            success: function(b) {
                img_src[a] =
                    b;
                drop_draw_image(a);
                $("#obj_" + a).html("");
                flag_load = !0;
                a++;
                a == grid_count + 1 && ($("#main_object_load").html(""), $("#main_object_load").css({
                    "z-index": "0"
                }), flag_load = !1);
                setTimeout(function() {
                    async(a)
                }, 1)
            }
        }) : (img_src[a] = $("div.div_img_rm").eq(b).attr("data-drag-img"), drop_draw_image(a), $("#obj_" + a).html(""), a++, a == grid_count + 1 && ($("#main_object_load").html(""), $("#main_object_load").css({
            "z-index": "0"
        })), setTimeout(function() {
            async(a)
        }, 1))
    }
}

function change_effect() {
    $("#main_object_load").css({
        "z-index": "2660"
    });
    $("#main_object_load").html('<div class="loding_div_now"><img style="margin-left:-64px;" class="loding_img_now" src="core-designer/img/ajax-loader-3.gif"></div>');
    flag_ef = !0;
    setTimeout(function() {
        formation_object(grid)
    }, 1E3);
    "1" !== effect ? setTimeout(function() {}, 3E3) : (flag_ef = !1, $("#main_object_load").html(""), $("#main_object_load").css({
        "z-index": "0"
    }), setTimeout(function() {
        flag_ef = !1
    }, 50))
}

function handleMouseDown(a, b) {
    canMouseX = parseInt(a.clientX - offsetX[b]);
    canMouseY = parseInt(a.clientY - offsetY[b]);
    X[b] = canMouseX - sX[b];
    Y[b] = canMouseY - sY[b];
    isDragging = !0
}

function handleMouseUp(a, b) {
    $(".obj_func_edit").show();
    isDragging = !1
}

function handleMouseOut(a, b) {
    isDragging = !1
}

function handleMouseMove(a, b) {
    $("#obj_" + b).css({
        cursor: "move"
    });
    canMouseX = parseInt(a.clientX - offsetX[b]);
    canMouseY = parseInt(a.clientY - offsetY[b]);
    isDragging && ($(".obj_func_edit").hide(), sX[b] = canMouseX - X[b], sY[b] = canMouseY - Y[b], sX[b] >= left_offset[b] && (sX[b] = left_offset[b]), sX[b] <= -left_offset[b] && (sX[b] = -left_offset[b]), sY[b] >= top_offset[b] && (sY[b] = top_offset[b]), sY[b] <= -top_offset[b] && (sY[b] = -top_offset[b]), design_ctx.clearRect(obj_left_offset[b], obj_top_offset[b], obj_width[b], obj_height[b]), design_ctx.drawImage(obj_image[b],
        left_offset[b] - sX[b], top_offset[b] - sY[b], img_width[b], img_height[b], obj_left_offset[b], obj_top_offset[b], obj_width[b], obj_height[b]))
}

function drag_up_append() {
    $("div.div_img_rm").draggable({
        helper: "clone",
        opacity: .9,
        cursor: "move",
        zIndex: 12,
        revert: !0
    })
}

function getCookie(a) {
    return (new RegExp("(?:; )?" + a + "=([^;]*);?")).test(document.cookie) ? decodeURIComponent(RegExp.$1) : !1
}

function recurse_ajax_insta() {
    $.ajax({
        type: "POST",
        url: "core-designer/social/insta/insta-ajax.php",
        data: "insta_token=" + getCookie("it1"),
        cache: !1,
        success: function(a) {
            $("#insta_photos").append(a);
            getCookie("next_url") ? recurse_ajax_insta() : document.cookie = "next_url=; path=/; expires=" + del.toUTCString()
        },
        error: function() {
            $("#insta_photos").html("\u041f\u0440\u043e\u0438\u0437\u043e\u0448\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430. \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u0435\u0440\u0435\u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0438 \u043f\u043e\u043f\u0440\u043e\u0431\u0443\u0439\u0442\u0435 \u0441\u043d\u043e\u0432\u0430. \u0412\u0441\u0435 \u0431\u0443\u0434\u0435\u0442 \u0440\u0430\u0431\u043e\u0442\u0430\u0442\u044c.")
        }
    })
}

function InstaAjax() {
    document.cookie = "next_url=; path=/; expires=" + del.toUTCString();
    $("#insta_photos").html('<div class="div_load_photos"><img src="core-designer/img/ajax-loader.gif" /> <br><br> \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u043e\u0434\u043e\u0436\u0434\u0438\u0442\u0435 ...</div>');
    $.ajax({
        type: "POST",
        url: "core-designer/social/insta/insta-ajax.php",
        data: "insta_token=" + getCookie("it1"),
        cache: !1,
        success: function(a) {
            $("#insta_photos").html(a);
            getCookie("next_url") &&
            recurse_ajax_insta()
        },
        error: function() {
            $("#insta_photos").html("\u041f\u0440\u043e\u0438\u0437\u043e\u0448\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430. \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u0435\u0440\u0435\u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0438 \u043f\u043e\u043f\u0440\u043e\u0431\u0443\u0439\u0442\u0435 \u0441\u043d\u043e\u0432\u0430. \u0412\u0441\u0435 \u0431\u0443\u0434\u0435\u0442 \u0440\u0430\u0431\u043e\u0442\u0430\u0442\u044c.")
        }
    })
}

function VkAjax() {
    $("#vk_photos").html('<div class="div_load_photos"><img src="core-designer/img/ajax-loader.gif" /> <br><br> \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u043e\u0434\u043e\u0436\u0434\u0438\u0442\u0435 ...</div>');
    $.ajax({
        type: "POST",
        url: "core-designer/social/vk/vk-ajax.php",
        data: "vk_token=" + getCookie("it2"),
        cache: !1,
        success: function(a) {
            $("#vk_photos").html(a)
        },
        error: function() {
            $("#vk_photos").html("\u041f\u0440\u043e\u0438\u0437\u043e\u0448\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430. \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u0435\u0440\u0435\u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0438 \u043f\u043e\u043f\u0440\u043e\u0431\u0443\u0439\u0442\u0435 \u0441\u043d\u043e\u0432\u0430. \u0412\u0441\u0435 \u0431\u0443\u0434\u0435\u0442 \u0440\u0430\u0431\u043e\u0442\u0430\u0442\u044c.")
        }
    })
}

function clear() {
    for (var a = -1; 50 < getCookie("i"); a++) document.cookie = "col" + a + "=; path=/; expires=" + del.toUTCString(), document.cookie = "mod" + a + "=; path=/; expires=" + del.toUTCString(), document.cookie = "mat" + a + "=; path=/; expires=" + del.toUTCString(), document.cookie = "k" + a + "=; path=/; expires=" + del.toUTCString(), document.cookie = "img" + a + "=; path=/; expires=" + del.toUTCString(), document.cookie = "name" + a + "=; path=/; expires=" + del.toUTCString();
    document.cookie = "i=; path=/; expires=" + del.toUTCString()
}

function preventSelection(a) {
    function b(a, b, c) {
        a.attachEvent ? a.attachEvent("on" + b, c) : a.addEventListener && a.addEventListener(b, c, !1)
    }

    function c() {
        window.getSelection ? window.getSelection().removeAllRanges() : document.selection && document.selection.clear && document.selection.clear()
    }

    function d(a) {
        a = a || window.event;
        if (!(a.target || a.srcElement).tagName.match(/INPUT|TEXTAREA/i)) {
            var b = a.keyCode || a.which;
            a.ctrlKey && 65 == b && (c(), a.preventDefault ? a.preventDefault() : a.returnValue = !1)
        }
    }
    var e = !1;
    b(a, "mousemove",
        function() {
            e && c()
        });
    b(a, "mousedown", function(a) {
        a = a || window.event;
        e = !(a.target || a.srcElement).tagName.match(/INPUT|TEXTAREA/i)
    });
    b(a, "mouseup", function() {
        e && c();
        e = !1
    });
    b(a, "keydown", d);
    b(a, "keyup", d)
}

function progressbar() {
    function a() {
        var c = b.progressbar("value") || 0;
        b.progressbar("value", c + 1);
        99 > c && setTimeout(a, 900)
    }
    var b = $("#progressbar"),
        c = $(".progress-label");
    b.progressbar({
        value: !1,
        change: function() {
            c.text(b.progressbar("value") + "%")
        },
        complete: function() {
            c.text("Complete!")
        }
    });
    setTimeout(a, 2E3)
}
var color_podushka = "\u0427\u0451\u0440\u043d\u044b\u0439";
$(".color-podushka").bind("click", function() {
    $(".color-podushka").removeClass("color-active");
    $(this).addClass("color-active");
    color_podushka = $(this).attr("id");
    color_podushka = "-color-black" === color_podushka ? "\u0427\u0451\u0440\u043d\u044b\u0439" : "\u0411\u0435\u043b\u044b\u0439"
});
$("#callmediv").hide();
$("#callme").bind("click", function() {
    $("#callmediv").slideDown(500)
});
$("#ok-call").bind("click", function() {
    $.ajax({
        type: "POST",
        url: "app/footer/ajax-callme.php",
        data: "client-name=" + $("#client-name").val() + "&client-tel=" + $("#client-tel").val() + "&client-comment=" + $("#client-comment").val(),
        cache: !1,
        success: function(a) {
            $("#callme").append(a);
            $("#callmediv").hide()
        }
    })
});
$("#ok").bind("click", function() {
    $.ajax({
        type: "POST",
        url: "app/footer/ajax-add-email.php",
        data: "client-email=" + $("#client-email").val(),
        cache: !1,
        success: function(a) {
            $("#mess-append-email").html(a)
        }
    })
});
$("#-dostavka-box").hide();
$("#-faq-box").hide();
$("#-about-box").hide();
$("#-oplata-box").hide();
$("#-reviews-box").hide();
$("#-koti-box").hide();
$("#-constructor-box").hide();
$("#-fon").hide();
$("#-fon-con").hide();
$("#-fon").css({
    "margin-top": "0px"
});
$("#-fon-con").css({
    "margin-top": "0px",
    "z-index": "1000009"
});
$("#-dostavka-box").css({
    "margin-top": "0px"
});
$("#-faq-box").css({
    "margin-top": "0px"
});
$("#-about-box").css({
    "margin-top": "0px"
});
$("#-koti-box").css({
    "margin-top": "0px"
});
$("#-oplata-box").css({
    "margin-top": "0px"
});
$("#-reviews-box").css({
    "margin-top": "0px"
});
$("#-constructor-box").css({
    "margin-top": "0px",
    "z-index": "1000009",
    top: "10px"
});
$(".-create-case").bind("click", function() {
    $("body").css("overflow", "hidden");
    $("#-fon-con").show();
    680 > document.body.clientHeight ? $("#-constructor-box").css({
        bottom: "-20px"
    }) : $("#-constructor-box").css({
        "margin-top": "-280px",
        top: "50%"
    });
    $("#-constructor-box").show(200)
});
$("#-top-href-fixed-shipping, #-footer-href-dostavka").bind("click", function() {
    $("body").css("overflow", "hidden");
    $("#-fon").show();
    $("#-dostavka-box").show(200)
});
$("#-top-href-fixed-payment, #-footer-href-oplata").bind("click", function() {
    $("body").css("overflow", "hidden");
    $("#-fon").show();
    $("#-oplata-box").show(200)
});
$("#-top-href-fixed-reviews, #-footer-href-reviews").bind("click", function() {
    $("body").css("overflow", "hidden");
    $(".-content-info-box").css({
        bottom: "50px"
    });
    $("#-fon").show();
    $("#-reviews-box").show(200)
});
$("#-footer-href-faq").bind("click", function() {
    $("body").css("overflow", "hidden");
    $(".-content-info-box").css({
        bottom: "50px"
    });
    $("#-fon").show();
    $("#-faq-box").show(200)
});
$("#-footer-href-about").bind("click", function() {
    $("body").css("overflow", "hidden");
    $("#-fon").show();
    $("#-about-box").show(200)
});
$("#-fon").bind("click", function() {
    $(".-content-info-box").hide();
    $(".-content-info-box").css({
        bottom: ""
    });
    $("#-fon").hide();
    $("body").css("overflow", "auto")
});
$(".-caption-content-info-box").bind("click", function() {
    $(".-content-info-box").hide();
    $(".-content-info-box").css({
        bottom: ""
    });
    $("#-fon").hide();
    $("#-fon-con").hide();
    $("body").css("overflow", "auto")
});
$("#seychas").bind("click", function() {
    $(".-content-info-box").hide();
    $(".-content-info-box").css({
        bottom: ""
    });
    $("#-fon").hide();
    $("#-fon-con").hide();
    $("body").css("overflow", "auto")
});
var userDeviceArray = [{
        device: "Android",
        platform: /Android/
    }, {
        device: "iPhone",
        platform: /iPhone/
    }, {
        device: "iPad",
        platform: /iPad/
    }, {
        device: "Symbian",
        platform: /Symbian/
    }, {
        device: "Windows Phone",
        platform: /Windows Phone/
    }, {
        device: "Tablet OS",
        platform: /Tablet OS/
    }],
    platform = navigator.userAgent;
getPlatform();

$("#designer").html('<div id="main_object"></div><div id="drag_photo_image"></div><div id="main_object_load"></div><canvas id="canvasdes"></canvas><canvas id="canvasrezult"></canvas>');
$(".rev_gor").hide();

$(".rev_gor").eq(0).show();
$(".rev_gor").append('<center><div class="more" style="font-size:18px;padding:20px;text-decoration:underline;cursor: pointer;">\u0415\u0449\u0451 \u043e\u0442\u0437\u044b\u0432\u044b</div></center>');
$(".more").bind("click", function() {
    $(".more").hide();
    $(".rev_gor").slideDown(1500)
});
$(".tips").hide();
$(".tips-top").hide();
$(".tips-right").hide();
$(".tips-center").hide();
$(".tips-card").hide();
$("#closetips").hide();
$("#showtips").bind("click", function() {
    $(".spb").slideUp();
    $(".tips").show();
    $(".tips-right").show();
    $(".tips-center").show();
    $(".tips-card").show();
    $(".tips-top").show();
    $("#showtips").hide();
    $("#closetips").show();
    return !1
});
$(document).bind("click", function() {
    $(".tips").is(":visible") && ($(".tips").hide(), $(".tips-top").hide(), $(".tips-right").hide(), $(".tips-center").hide(), $(".tips-card").hide(), $("#closetips").hide(), $("#showtips").show())
});
var path_models = "core-designer/img/models/",
    design_ctx = document.getElementById("canvasdes").getContext("2d"),
    rezult_ctx = document.getElementById("canvasrezult").getContext("2d"),
    koef_canvas = 1,
    w_device = 205 * koef_canvas,
    h_device = 434 * koef_canvas,
    W = 0,
    H = 0,
    imgBg = new Image,
    device = "iphone5",
    color = "black_",
    material = "plastic",
    grid = "4",
    effect = "1",
    flag_auto_bottom = !0,
    eq = 1,
    border_color_hover = "blue",
    border_color = "#ccc",
    img_src = [],
    obj_width = [],
    obj_height = [],
    obj_image = [],
    left_offset = [],
    top_offset = [],
    img_width = [],
    img_height = [],
    obj_left_offset = [],
    obj_top_offset = [],
    sX = [],
    sY = [],
    X = [],
    Y = [],
    offsetX = [],
    offsetY = [],
    date = new Date,
    del = new Date(0),
    grid_count = 4,
    koef_zoom_plus = .95,
    koef_zoom_minus = 1.05,
    price_phone = "",
    price_tablet = "",
    price_phone_silicon = "",
    rub = '<p style="font-size:32px;margin-top:4px;"></p>',
    isEdit = [],
    flag_load = !1,
    flag_ef = !1,
    flag_gr = !0,
    flag_vk_click = !1,
    flag_insta_click = !1,
    flag_drag_photo = !1,
    isDragging = !1;
date.setDate(date.getDate() + 15552000);
document.cookie = "next_url=; path=/; expires=" + del.toUTCString();
for (var z = 1; 26 > z; z++) img_src[z] = "";
for (z = 1; 26 > z; z++) obj_width[z] = 0;
for (z = 1; 26 > z; z++) obj_height[z] = 0;
for (z = 1; 26 > z; z++) left_offset[z] = 0;
for (z = 1; 26 > z; z++) top_offset[z] = 0;
for (z = 1; 26 > z; z++) img_width[z] = 0;
for (z = 1; 26 > z; z++) img_height[z] = 0;
for (z = 1; 26 > z; z++) obj_image[z] = new Image;
for (z = 1; 26 > z; z++) obj_left_offset[z] = 0;
for (z = 1; 26 > z; z++) obj_top_offset[z] = 0;
for (z = 1; 26 > z; z++) sX[z] = 0;
for (z = 1; 26 > z; z++) sY[z] = 0;
for (z = 1; 26 > z; z++) X[z] = 0;
for (z = 1; 26 > z; z++) Y[z] = 0;
for (z = 1; 26 > z; z++) offsetX[z] = 0;
for (z = 1; 26 > z; z++) offsetY[z] = 0;
for (z = 1; 26 > z; z++) isEdit[z] = !1;
$(function() {
    $("#canvasdes").attr({
        width: "502",
        height: "502"
    });
    $("#canvasdes").css({
        top: "0",
        left: "0",
        width: "502",
        height: "502"
    });
    $("#main_object").css({
        top: "0",
        left: "0",
        width: "500",
        height: "500"
    });
    w_device = 502 * koef_canvas;
    h_device = 502 * koef_canvas;
    four_squares_grid();
    $("#price").html(price_phone + rub);
    $(".spb").hide();
    $(".div_photos").hide();
    $(".spb:first").slideDown("slow");
    $("#upload_photos").show();
    $(".spt").bind("click", function() {
        flag_auto_bottom = !1;
        $(this).next(".spb").is(":visible") ? $(".spb").slideUp("slow") :
            ($(".spb").slideUp("slow"), $(this).next(".spb").slideDown("slow"))
    })
});

$("li").bind("click", function(e) {
    var source = $(e.target);
    var sourceId = source.parent().parent().attr('id');
	if (sourceId == 'grid') {
        $('#background-image').attr('src', 'img/podushka.png');
		f = "square";
    }
    if (sourceId == 'grid_one') {
        $('#background-image').attr('src', 'img/podushka3.png');
		f = "heart";
    }
    if (sourceId == 'grid_two') {
        $('#background-image').attr('src', 'img/podushka2.png');
		f = "circle";
    }

    $(this).attr("data-device") && !0 === flag_gr && (device = $(this).attr("data-device"), "ipad_mini" === device || "ipad-2-3-4" === device || "ipad_air" === device ? $("#price").html(price_tablet + rub) : $("#price").html(price_phone + rub), w_device = $(this).attr("data-w") * koef_canvas, h_device = $(this).attr("data-h") * koef_canvas, $("#canvasdes").attr({
        width: w_device,
        height: h_device
    }), $("#canvasdes").css({
        top: $(this).attr("data-t"),
        left: $(this).attr("data-l"),
        width: $(this).attr("data-w"),
        height: $(this).attr("data-h")
    }), $("#name_spt_dev").html($(this).attr("data-name")), $("#main_object").css({
        top: $(this).attr("data-t"),
        left: $(this).attr("data-l"),
        width: $(this).attr("data-w"),
        height: $(this).attr("data-h")
    }), $("#black_plastic").hide(), $("#white_plastic").hide(), $("#transparent_plastic").hide(), $("#white_silicon").hide(), $("#black_silicon").hide(), $("#transparent_silicon").hide(), $("#name_spt_colmat").html("\u0427\u0435\u0440\u043d\u044b\u0439 \u041f\u043b\u0430\u0441\u0442\u0438\u043a"), color = "black_", $(this).attr("data-black-plastic") &&
    $("#black_plastic").show(), $(this).attr("data-white-plastic") && $("#white_plastic").show(), $(this).attr("data-transparent-plastic") && $("#transparent_plastic").show(), $(this).attr("data-black-silicon") && $("#black_silicon").show(), $(this).attr("data-white-silicon") && $("#white_silicon").show(), $(this).attr("data-transparent-silicon") && $("#transparent_silicon").show());
    $(this).attr("data-color") && (color = $(this).attr("data-color"), material = $(this).attr("data-material"), "silicon" === material ? $("#price").html(price_phone_silicon +
    rub) : "ipad_mini" === device || "ipad-2-3-4" === device || "ipad_air" === device ? $("#price").html(price_tablet + rub) : $("#price").html(price_phone + rub), $("#name_spt_colmat").html($(this).attr("data-name")));
    $(this).attr("data-grid") && !0 === flag_gr && (grid = $(this).attr("data-grid"), $("#name_spt_grid").html($(this).attr("data-name")));
    $(this).attr("data-num-effect") && !1 === flag_ef && (effect = $(this).attr("data-num-effect"), $("#name_spt_effect").html($(this).attr("data-effect")), change_effect());
    ($(this).attr("data-grid") ||
    $(this).attr("data-device")) && !0 === flag_gr && (flag_gr = !1, $("#main_object_load").css({
        "z-index": "2660"
    }), $("#main_object_load").html('<div class="loding_div_now"><img style="margin-left:-64px;" class="loding_img_now" src="core-designer/img/ajax-loader-3.gif"></div>'), formation_object(grid), "" == img_src[1] ? ($("#main_object_load").html(""), $("#main_object_load").css({
        "z-index": "0"
    }), flag_gr = !0) : setTimeout(function() {
        $("#main_object_load").html("");
        $("#main_object_load").css({
            "z-index": "0"
        });
        setTimeout(function() {
            flag_gr = !0
        }, 50)
    }, 0), effect = 1, $("#name_spt_effect").html("\u0411\u0435\u0437 \u0444\u0438\u043b\u044c\u0442\u0440\u0430"))
});
$("#random").bind("click", function() {
    $("#main_object_load").css({
        "z-index": "2660"
    });
    $("#main_object_load").html('<div class="loding_div_now"><img style="margin-left:-64px;" class="loding_img_now" src="core-designer/img/ajax-loader-3.gif"></div>');
    effect = 1;
    $("#name_spt_effect").html("\u0411\u0435\u0437 \u0444\u0438\u043b\u044c\u0442\u0440\u0430");
    for (var a = 1; a < grid_count + 1; a++) $("#obj_" + a).html('<div class="loding_div_now"><img class="loding_img_now" src="core-designer/img/ajax-loader-2.gif"></div>');
    async(1)
});
$("#clear_des").bind("click", function() {
    for (var a = 1; 26 > a; a++) img_src[a] = "";
    formation_object(grid)
});
drag_up_append();
flag_drag_photo = !0;
window.fbAsyncInit = function() {
    FB.init({
        appId: "290777361104242",
        cookie: !0,
        xfbml: !0,
        channelUrl: "http://podushki1.ru/fb-access.php",
        oauth: !0
    })
};


/*(function() {
    var a = document.createElement("script");
    a.async = !0;
    a.src = document.location.protocol + "//connect.facebook.net/en_US/all.js";
    document.getElementById("fb-root").appendChild(a)
})();
*/

$("#auth_fb").bind("click", function() {
    FB.login(function(a) {
        "connected" === a.status && ($("#fb_photos").html('<div class="div_load_photos"><img src="core-designer/img/ajax-loader.gif" /> <br><br> \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u043e\u0434\u043e\u0436\u0434\u0438\u0442\u0435 ...</div>'), $("#fb_photos").load("fb-ajax.php"))
    }, {
        scope: "email,user_birthday,user_photos,offline_access"
    })
});
$("#auth_vk").bind("click", function() {
    window.open("http://oauth.vk.com/authorize?client_id=4665707&redirect_uri=http://podushki1.ru/vk-acess.php&scope=photos&display=popup&response_type=token&v=5.27", "mywindow", "width=400,height=300")
});
$("#auth_insta").bind("click", function() {
    window.open("https://instagram.com/oauth/authorize/?client_id=27f5f6be12b4494a9107edd305036940&redirect_uri=http://podushki1.ru/insta-access.php&response_type=token", "mywindow", "width=400,height=300")
});
$("#insta_div").bind("click", function() {
    $(".social_div").removeClass("active");
    $("#insta_div").addClass("active");
    $(".div_photos").hide();
    $("#insta_photos").show()
});
$("#vk_div").bind("click", function() {
    $(".social_div").removeClass("active");
    $("#vk_div").addClass("active");
    $(".div_photos").hide();
    $("#vk_photos").show()
});
$("#fb_div").bind("click", function() {
    $(".social_div").removeClass("active");
    $("#fb_div").addClass("active");
    $(".div_photos").hide();
    $("#fb_photos").show()
});
$("#upload_div").bind("click", function() {
    $(".social_div").removeClass("active");
    $("#upload_div").addClass("active");
    $(".div_photos").hide();
    $("#upload_photos").show()
});
var ua = navigator.userAgent.toLowerCase(); - 1 != ua.indexOf("safari") && ua.indexOf("chrome");
var uploader = new plupload.Uploader({
    multipart: !0,
    multi_selection: !0,
    multiple_queues: !0,
    runtimes: "gears,html5,flash,silverlight,html4",
    browse_button: "pickfiles",
    drop_element: "content",
    max_retries: 10,
    container: document.getElementById("container"),
    url: "upload.php",
    flash_swf_url: "js/Moxie.swf",
    silverlight_xap_url: "js/Moxie.xap",
    unique_names: !1,
    filters: {
        mime_types: [{
            title: "Image files",
            extensions: "jpg,jpeg,gif,png"
        }]
    },
    resize: {
        width: 1500,
        height: 1500,
        quality: 100,
        crop: false
    },
    init: {
        PostInit: function() {
            document.getElementById("filelist").innerHTML =
                "";
            document.getElementById("uploadfiles").onclick = function() {
                uploader.start();
                return !1
            }
        },
        FilesAdded: function(a, b) {
            $("#mess_del").hide();
            $("#pickfiles").html("\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044c \u0435\u0449\u0435 ...");
            $("#pickfiles").css({
                "margin-top": "5px"
            });
            plupload.each(b, function(a) {
                document.getElementById("filelist").innerHTML += '<div id="' + a.id + '">' + a.name + " (" + plupload.formatSize(a.size) + ") <b></b></div>"
            })
        },
        QueueChanged: function() {
            uploader.start();
            return !1
        },
        UploadProgress: function(a,
                                 b) {
            document.getElementById(b.id).getElementsByTagName("b")[0].innerHTML = "<span> " + b.percent + "%</span>"
        },
        FileUploaded: function(a, b) {
            document.getElementById(b.id).innerHTML = "";
            var c = $('<div class="div_img_rm" data-drag-img="uploads/' + b.name + '"><img class="img_rm" src="uploads/' + b.name + '"></div>');
            $("#img_up_rm").append(c);
            drag_up_append()
        },
        Error: function(a, b) {
            document.getElementById("console").innerHTML +=
                "\nError #" + b.code + ": " + b.message
        }
    }
});
uploader.init();

//almsit_0303

$("#save_design").bind("click", function() {
	for (var a = 0, b = 1; b < grid_count + 1; b++) "" === img_src[b] && (a += 1);
    0 === a ? (koef_canvas = 8, w_device *= koef_canvas, h_device *= koef_canvas, $("#canvasrezult").attr({
        width: w_device,
        height: h_device
    }), 0 == getCookie("i") && (document.cookie = "i=0; path=/; expires=" + date.toUTCString()), b = getCookie("i"), b++, document.cookie = "i=" + b + "; path=/; expires=" + date.toUTCString(), document.cookie = "k" + getCookie("i") + "=1; path=/; expires=" + date.toUTCString(), document.cookie = "color" + getCookie("i") +
    "=" + color_podushka + "; path=/; expires=" + date.toUTCString(), formation_object(grid), setTimeout(function() {
        save()
    }, 6000)) : ($("body").css("overflow", "hidden"), $("#-fon").show(), $("#-koti-box").show(200))
});


preventSelection(document);